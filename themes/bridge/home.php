
<?php get_header() ?>
<section class="background-white">
	<div class="row home-background pos-rel">
		<div class="hero-text">
			<p class="clearfix-p home-make text-center">Make Your <b class="fr-wf">Frontline Workforce</b> more...</p>
			<p class="clearfix-p productive-engaged text-center">...Productive, Engaged and Skilled</p>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 service-cards clearfix-col">
			<div class="service-card" onclick="window.location.href='/sales-acceleration'">
				<p class="card-header clearfix-p">NEXT-GEN</p>
				<p class="card-text clearfix-p">Sales Acceleration Platform </p>
				<p class="card-sub-text right-arrow">for Sales Frontline</p>
			</div>
			<div class="service-card" onclick="window.location.href='/back-office-performance-management'">
				<p class="card-header clearfix-p">NEXT-GEN</p>
				<p class="card-text clearfix-p">Performance Acceleration Platform</p>
				<p class="card-sub-text right-arrow">for Back-Office Frontline (Ops, Call-Center)</p>
			</div>
			<div class="service-card" onclick="window.location.href='/frontline-learning'">
				<p class="card-header clearfix-p">NEXT-GEN</p>
				<p class="card-text clearfix-p">Learning Management Platform</p>
				<p class="card-sub-text right-arrow">for All Frontline</p>
			</div>
			<div class="service-card" onclick="window.location.href='/artificial-intelligence-bots'">
				<p class="card-header clearfix-p">BESPOKE</p>
				<p class="card-text clearfix-p">Custom-built Solutions </p>
				<p class="card-sub-text bespoke right-arrow">(e.g., Digital Sales / Payment Solutions for Messaging <br/>Platforms such as whatsapp, wechat etc.)</p>
			</div>
			<div class="bots">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/bot_icon.png" class="mw-100 responsive-img">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-lg-12 hidden-xs" style="background-color: #fafafa;">
			<p class="clearfix-p text-center home-bank-clients">OUR CLIENTS</p>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 clearfix-col home-banks hidden-xs">
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/adib-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/rakbank-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/careem-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/tanfeeth-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/meeras-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/mashreq-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/emaar-h-96-1.png" class="responsive-img bank h-48">
			</div>
		</div>
		<div id="banks-carousel" class="carousel slide hidden-sm hidden-xl hidden-lg hidden-md nav-hide" data-ride="carousel" style="background-color: #fafafa;">
		    <div class="col-xs-12" style="background-color: #fafafa;">
				<p class="clearfix-p text-center home-bank-clients">OUR CLIENTS</p>
			</div>
		    <!-- Indicators -->
		    <ol class="carousel-indicators bank-carousel mob-bank-carousel">
		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		      <li data-target="#myCarousel" data-slide-to="1"></li>
		      <li data-target="#myCarousel" data-slide-to="2"></li>
		      <li data-target="#myCarousel" data-slide-to="3"></li>
		      <li data-target="#myCarousel" data-slide-to="4"></li>
		      <li data-target="#myCarousel" data-slide-to="5"></li>
		      <li data-target="#myCarousel" data-slide-to="6"></li>
		      <li data-target="#myCarousel" data-slide-to="7"></li>
		    </ol>

		    <!-- Wrapper for slides -->
		    <div class="carousel-inner text-center">
		      	<div class="item active">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/adib-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/rakbank-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/careem-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/tanfeeth-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/meeras-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/mashreq-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/emaar-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		    </div>
		</div>
	</div>
	<div class="row bit-pla background-white clearfix-row">
		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 clearfix-col">
			<p class="clearfix-p bit-pla-text home-main-text text-center">Core BitNudge Platform</p>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 clearfix-col">
			<div class="col-md-3 col-sm-3 col-xs-12 col-lg-3 clearfix-col pr-3">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/core_img01.png" class="mw-100 responsive-img">
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 col-lg-3 clearfix-col pr-3">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/core_img02.png" class="mw-100 responsive-img">
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 col-lg-3 clearfix-col pr-3">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/core_img03.png" class="mw-100 responsive-img">
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 col-lg-3 clearfix-col">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/core_img04.png" class="mw-100 responsive-img">
			</div>
		</div>
	</div>
	<div class="row background-white clearfix-row functional">
		<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 clearfix-p">
			<p class="clearfix-p home-main-text text-center functional-text">Functional Use Cases</p>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 ml-35">
			<div class="col-xs-12 col-md-4 col-sm-4 col-lg-4 clearfix-col">
				<div id="functional-sales" class="carousel slide br-functional-use-case w-560 h-716" data-ride="carousel" data-interval="false">
				    <!-- Indicators -->
				    <ol class="carousel-indicators bank-carousel functional-indicator">
				      <li data-target="#functional-sales" data-slide-to="0" class="active"></li>
				      <li data-target="#functional-sales" data-slide-to="1"></li>
				    </ol>
				    <!-- Wrapper for slides -->
				    <div class="carousel-inner">
				      	<div class="item active">
					        <div>
					        	<div class="bg-sales bg-functional-headrer v-h-align">
					        		<p class="clearfix-p functional-img-heading" style="margin: 0px;">Sales</p>
					        	</div>
					        	<div>
					        		<p class="clearfix-p customizable-use-case">Our customizable Sales management platform improves the performance of sales workforce by leveraging the power of Advanced Analytics, Technology and, Gamification.</p>
					        	</div>	
					        </div>
				      	</div>
				      	<div class="item">
					        <div>
					        	<div class="bg-sales bg-functional-headrer v-h-align">
					        		<p class="clearfix-p functional-img-heading"  style="margin: 0px;">Sales</p>
					        	</div>
					        	<div>
					        		<ul class="operations-list operations-use-case sales-use-case2">
					        		<li>Virtual voice bots assisting frontline in day <span class="ml-25-use-case">to day sales</span></li>
				        			<li>Engagement tracking of Customers and <span class="ml-25-use-case">digital onboarding</span></li>
				        			<li>Intelligent Lead and Activity Management </li>
				        			<li>Planning engine </li>
				        			<li>Incentives management </li>
				        			<li>Automation and Analytics </li>
				        			<li>Gamification (competition/ contest, etc.) </li>
				        			<li>Learning </li>
				        		</ul>
					        	</div>	
					        </div>
				      	</div>
				    </div>
			  	</div>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-4 col-lg-4 clearfix-col">
				<div id="functional-operations" class="carousel slide br-functional-use-case w-560 h-716" data-ride="carousel" data-interval="false">
				    <!-- Indicators -->
				    <ol class="carousel-indicators bank-carousel functional-indicator">
				      <li data-target="#functional-operations" data-slide-to="0" class="active"></li>
				      <li data-target="#functional-operations" data-slide-to="1"></li>
				    </ol>
				    <!-- Wrapper for slides -->
				    <div class="carousel-inner">
				      <div class="item active">
				        <div>
				        	<div class="bg-operations bg-functional-headrer v-h-align">
				        		<p class="clearfix-p functional-img-heading" style="margin: 0px;">Operations</p>
				        	</div>
				        	<div>
				        		<div>
					        		<p class="clearfix-p customizable-use-case">BitNudge tracks the granular activities of the frontline employees in operations to calculate the overall performance on the basis of activities and achievement, for example, Workitems processed, Accuracy, Turn-around-time etc.</p>
					        	</div>	
				        	</div>	
				        </div>
				      </div>
				      <div class="item">
				        <div>
				        	<div class="bg-operations bg-functional-headrer v-h-align">
				        		<p class="clearfix-p functional-img-heading" style="margin: 0px;">Operations</p>
				        	</div>
				        	<div>
				        		<ul class="operations-list operations-use-case">
				        			<li>Planning/ Forecasting engine </li>
				        			<li>Intelligent Queue management </li>
				        			<li>Gamification (competition, campaign, <span class="ml-25-use-case">badges, SWOT etc.)</span> </li>
				        			<li>Incentives and compensation management </li>
				        			<li>Learning </li>
				        			<li>Automation and Analytics </li>
				        		</ul>
				        	</div>	
				        </div>
				      </div>
				    </div>
			  	</div>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-4 col-lg-4 clearfix-col">
				<div id="functional-call" class="carousel slide br-functional-use-case w-560 h-716" data-ride="carousel" data-interval="false">
				    <!-- Indicators -->
				    <ol class="carousel-indicators bank-carousel functional-indicator">
				      <li data-target="#functional-call" data-slide-to="0" class="active"></li>
				      <li data-target="#functional-call" data-slide-to="1"></li>
				    </ol>
				    <!-- Wrapper for slides -->
				    <div class="carousel-inner">
				      	<div class="item active">
					        <div>
					        	<div class="bg-call-center bg-functional-headrer v-h-align">
					        		<p class="clearfix-p functional-img-heading" style="margin: 0px;">Call Center</p>
					        	</div>
					        	<div>
					        		<p class="clearfix-p customizable-use-case">BitNudge boosts the performance of the Call center agents by engaging them in a gamified performance management regime. BitNudge awards points and badges to the top performers and creates an atmosphere of healthy competition among call center agents.</p>
					        	</div>	
					        </div>
				      	</div>
				      	<div class="item">
					        <div>
					        	<div class="bg-call-center bg-functional-headrer v-h-align">
					        		<p class="clearfix-p functional-img-heading" style="margin: 0px;">Call Center</p>
					        	</div><div>
				        		<ul class="operations-list operations-use-case">
				        			<li>Planning </li>
				        			<li>Gamification (competition, campaign, <span class="ml-25-use-case">badges, SWOT etc.)</span></li>
				        			<li>Incentives and compensation management </li>
				        			<li>Learning </li>
				        			<li>Analytics </li>
				        		</ul>
				        	</div>	
					        </div>
				      	</div>
				    </div>
			  	</div>
			</div>
		</div>
	</div>
	<div class="row background-ghost-white v-align bg-bitnudge clearfix-row">
		<div class="col-md-12 col-xs-12 xol-sm-12 col-lg-12 innovation-core clearfix-col mlr-70">
			<div class="pb-90">
				<p class="clearfix-p text-center features-core">Bitnudge - Innovation at the core</p>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 mb-74">
				<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 v-align" onclick="window.location.href='http://gamechangesns.com/lead-management/'">
					<div class="home-features"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/lead_management_icon-1.png" class="mw-100">
					</div>
					<span class="clearfix-p text-center">Lead Management</span>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 v-align" onclick="window.location.href='http://gamechangesns.com/gamification/'">
					<div class="home-features"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/gamification_icon-1.png" class="mw-100">
					</div>
					<span class="clearfix-p text-center">Gamification</span>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 v-align" onclick="window.location.href='http://gamechangesns.com/social-selling/'">
					<div class="home-features"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/social_selling_icon-1.png" class="mw-100">
					</div>
					<span class="clearfix-p text-center">Social Selling and Digital Onboarding</span>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 mb-74">
				<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 v-align" onclick="window.location.href='http://gamechangesns.com/lead-scoring-and-allocation/'">
					<div class="home-features"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/lead_scoring_icon-1.png" class="mw-100">
					</div>
					<span class="clearfix-p text-center">Lead Scoring and Allocation</span>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 v-align" onclick="window.location.href='http://gamechangesns.com/recommendation-engine/'">
					<div class="home-features"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/recommendation_icon-1.png" class="mw-100">
					</div>
					<span class="clearfix-p text-center">Recommendation Engine</span>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 v-align" onclick="window.location.href='http://gamechangesns.com/geo-tagging/'">
					<div class="home-features"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/geo_tagging_icon-1.png" class="mw-100">
					</div>
					<span class="clearfix-p text-center">Geo-tagging/ location service</span>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 mb-74">
				<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 v-align" onclick="window.location.href='http://gamechangesns.com/performance-management/'">
					<div class="home-features"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/performance_management_icon-1.png" class="mw-100">
					</div>
					<span class="clearfix-p text-center">Performance Management</span>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 v-align" onclick="window.location.href='http://gamechangesns.com/social-channel/'">
					<div class="home-features"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/enterprise_social_icon-1.png" class="mw-100">
					</div>
					<span class="clearfix-p text-center">Enterprise Social Channel and Rewards</span>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 v-align" onclick="window.location.href='http://gamechangesns.com/frontline-learning/'">
					<div class="home-features"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/learning_management_icon-1.png" class="mw-100">
					</div>
					<span class="clearfix-p text-center">Frontline learning Management</span>
				</div>
			</div>
		</div>
	</div>
	<div class="stats-background w-100">
		<div class="p-50">
			<p class="clearfix-p text-center usage-statistics">Our Usage Statistics </p>
		</div>
		<div class="stats v-align">
			<div class="br-stats">
				<div class="text-right pr-100">
					<p class="stats-text mb-50">Daily Active Users</p>
					<p class="stats-count">4,500</p>
				</div>
			</div>
			<div class="br-stats">
				<div class="text-center">
					<p class="stats-text mb-50">Users Served Since Inspection</p>
					<p class="stats-count">35,000</p>
				</div>
			</div>
			<div class="">
				<div class="text-left pl-100">
					<p class="stats-text text-left mb-50 hide-mob">Hours of content served through the platform</p>
					<p class="stats-text text-left mb-50 show-mob">Hours of content served</p>
					<p class="stats-count text-left">2,000</p>
				</div>
			</div>
		</div>
		<div class="p-50">
			<p class="clearfix-p text-center usage-statistics">and counting </p>
		</div>
	</div>
	<div class="row pt-100 bg-blog">
		<div> 
			<p class="clearfix-p text-center black home-blog-posts mb-100">Blog Posts</p>
		  	<?php echo do_shortcode('[wonderplugin_carousel id="5"]'); ?>
		</div>
	</div>
	<div class="row background-white ptb-180-130 our-team">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<p class="clearfix-p text-center black home-blog-posts mb-100">Our Team</p>
		</div>
		<div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 text-center">
			<div class="our-team-mem">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic_01-1.png" class="responsive-img mw-100">
			</div>
			<p class="clearfix-p black team-text text-center">Shailesh Tiwari</p>
			<p class="clearfix-p text-center mtb-10-20 font18" style="color:#666666;">Founder and CEO </p>
			<p class="clearfix-p black text-center team-desc">Ex McKinsey and Company Associate Partner; Led a key segment of Banking Practice for McKinsey and Company in Middle East. Over 10 years of deep industry expertise in Financial Services covering major verticals of Banking; core skills around Digital, Performance Transformation.</p>
			<div class="v-align">
				<!-- <div class="social-icons mr-7" onclick="window.location.href='https://www.facebook.com/tiwari.shail?fref=ts'">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/facebook-1.png" class="mw-100 responsive-img">
				</div> -->
				<div class="social-icons mr-7" onclick="window.location.href='https://www.linkedin.com/in/shaileshtiwari'">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100 responsive-img">
				</div>
				<!-- <div class="social-icons" onclick="window.location.href=''">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/twitter-1.png" class="mw-100 responsive-img">
				</div> -->
			</div>
		</div>
		<div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 text-center">
			<div class="our-team-mem">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic_02.png" class="responsive-img mw-100">
			</div>
			<p class="clearfix-p black team-text text-center">Adhityan KV</p>
			<p class="clearfix-p text-center mtb-10-20 font18" style="color:#666666;">Chief Technology Officer</p>
			<p class="clearfix-p black text-center team-desc adityan">Led the web engineering team of Zomato, through their series D and E (attracting investments of more than $500 million). Led payments engineering for OpenTable - a billion dollar tech venture based out of San Francisco. Was world's youngest certified professional windows application developer; won a Guiness record for the same.</p>
			<div class="v-align">
				<!-- <div class="social-icons mr-7" onclick="window.location.href=''">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/facebook-1.png" class="mw-100 responsive-img">
				</div> -->
				<div class="social-icons mr-7" onclick="window.location.href='https://www.linkedin.com/in/adhityan'">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100 responsive-img">
				</div>
				<!-- <div class="social-icons" onclick="window.location.href=''">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/twitter-1.png" class="mw-100 responsive-img">
				</div> -->
			</div>
		</div>
		<div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 text-center">
			<div class="our-team-mem">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic_03.png" class="responsive-img mw-100">
			</div>
			<p class="clearfix-p black team-text text-center">Sumedh Wahane</p>
			<p class="clearfix-p text-center mtb-10-20 font18" style="color:#666666;">Chief Data Officer</p>
			<p class="clearfix-p black text-center team-desc">Transformation and Analytics Specialist with deep Consulting and Advanced Analytics experience at McKinsey. Solid track record of delivering high impact solutions at Global clients. Core skills around machine learning and AI.</p>
			<div class="v-align">
				<!-- <div class="social-icons mr-7" onclick="window.location.href='https://www.facebook.com/sumedh.wahane?fref=ts'">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/facebook-1.png" class="mw-100 responsive-img">
				</div> -->
				<div class="social-icons mr-7" onclick="window.location.href='https://www.linkedin.com/in/sumedhwahane'">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100 responsive-img">
				</div>
				<!-- <div class="social-icons" onclick="window.location.href='https://twitter.com/sumedh111'">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/twitter-1.png" class="mw-100 responsive-img">
				</div> -->
			</div>
		</div>
		<div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 text-center">
			<div class="our-team-mem">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic_04.png" class="responsive-img mw-100">
			</div>
			<p class="clearfix-p black team-text text-center">Mohit Jandwani</p>
			<p class="clearfix-p text-center mtb-10-20 font18" style="color:#666666;">India Head </p>
			<p class="clearfix-p black text-center team-desc lh-mohit">Tech leader with proven expertise in building and running high quality tech teams. CTO at Babychakra, which is the largest network of mothers in India. Lead the POS initiative at Limetray which is the largest restaurant software provider in India. IIT Delhi graduate and top ranker in JEE, India's most competitive exam.</p>
			<div class="v-align">
				<!-- <div class="social-icons mr-7" onclick="window.location.href=''">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/facebook-1.png" class="mw-100 responsive-img">
				</div> -->
				<div class="social-icons mr-7" onclick="window.location.href='https://www.linkedin.com/in/mohitjandwani'">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100 responsive-img">
				</div>
				<!-- <div class="social-icons" onclick="window.location.href=''">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/twitter-1.png" class="mw-100 responsive-img">
				</div> -->
			</div>
		</div>
	</div>
	<div class="row bg-learning text-center">
		<p class="clearfix-p text-center lr mt-270">Learning Resources</p>
		<div class="owl-carousel">
			<div data-id="t__NoFstCmQ" class="youtube-player">
			</div>
			<div data-id="0MD4Ymjyc2I" class="youtube-player">
			</div>
			<div data-id="GmGCTwaXJj8" class="youtube-player">
			</div>
			<div data-id="t__NoFstCmQ" class="youtube-player">
			</div>
			<div data-id="0MD4Ymjyc2I" class="youtube-player">
			</div>
			<div data-id="GmGCTwaXJj8" class="youtube-player">
			</div>
			<div data-id="t__NoFstCmQ" class="youtube-player">
			</div>
			<div data-id="0MD4Ymjyc2I" class="youtube-player">
			</div>
			<div data-id="GmGCTwaXJj8" class="youtube-player">
			</div>
		</div>
	</div>
	<div class="row bg-contact text-center" id="contact">
		<p class="clearfix-p text-center contact-text">Contact us for a <strong>free consultation</strong></p>
		<p class="clearfix-p text-center contact-sec-text">Dedication, Innovation, Value-add and everything in between.</p>
		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 mb-20">
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<p class="vis-hid">hai</p>
			</div>
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<div class="contact-form required">
					<label>Your Name</label><br/>
					<input type="text" class="name">
				</div>
				<div class="name-tooltip">
				</div>
			</div>
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<div class="contact-form required">
					<label>Subject</label><br/>
					<input type="text" class="subject">
				</div>
				<div class="subject-tolltip">
				</div>
			</div>
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<p class="vis-hid">hai</p>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 mb-20">
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<p class="vis-hid">hai</p>
			</div>
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<div class="contact-form required">
					<label>Email</label><br/>
					<input type="email" class="contact-email">
				</div>
				<div class="email-tooltip">
				</div>
			</div>
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<div class="contact-form required">
					<label>Your Message</label><br/>
					<textarea class="message"></textarea>
				</div>
				<div class="message-tolltip">
				</div>
			</div>
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<p class="vis-hid">hai</p>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<p class="vis-hid">hai</p>
			</div>
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
					<p class="vis-hid">hai</p>
				</div>
			</div>
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<div class="contact-form" style="text-align:right;" onclick="sendContactMail();">
					<button class="submit">Submit</button>
				</div>
			</div>
			<div class="col-sm-3 col-xs-12 col-lg-3 col-md-3">
				<p class="vis-hid">hai</p>
			</div>
		</div>
		<div class="mb-20 common-tooltip">
		</div>
	</div>
	<div class="row bg-clients">
		<div> 
			<p class="clearfix-p text-center black home-blog-posts mtb-130">Our Happy Clients</p>
			<div id="happy-clients" class="carousel slide" data-ride="carousel" data-interval="false">
			    <!-- Indicators -->
			    <ol class="carousel-indicators bank-carousel blog-carousel">
			      <li data-target="#happy-clients" data-slide-to="0" class="active"></li>
			      <li data-target="#happy-clients" data-slide-to="1"></li>
			    </ol>

			    <!-- Wrapper for slides -->
			    <div class="carousel-inner">
			      	<div class="item active">
			      		<div class="bg-lorem pos-rel">
			      			<div class="lorem-pos black text-center">
			      				<p class="clearfix-p black lorem-text">Working with GameChange Solutions has been a truly digital and innovative experience. The team has the potential to deliver business value with the most innovative use of the technology solutions. We look forward to working with GameChange Solutions and will recommend the same to other companies who have an appetite for growth.</p>
			      				<!-- <img src="http://159.89.156.150/wp-content/uploads/2018/01/mashreq-color.png" class="mw-100 responsive-img"> -->
			      			</div>
			      		</div>
			      	</div>
			      	<div class="item">
			      		<div class="bg-lorem pos-rel">
			      			<div class="lorem-pos black text-center">
			      				<p class="clearfix-p black lorem-text">GameChange Solution's BitNudge application allows all sales users to use all CRM functionalities from the field, allowing the sales agents to be on-the ground for most of the time in the day. Productivity of my Direct Sales team has gone up by more than 10% during a three month pilot of BitNudge</p>
			      				<!-- <img src="http://159.89.156.150/wp-content/uploads/2018/01/careem-color.png" class="mw-100 responsive-img"> -->
			      			</div>
			      		</div>
			      	</div>
			      	<!-- <div class="item">
			      		<div class="bg-lorem pos-rel">
			      			<div class="lorem-pos black text-center">
			      				<p class="clearfix-p black lorem-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			      				<img src="http://159.89.156.150/wp-content/uploads/2018/01/adib-color.png" class="mw-100 responsive-img">
			      			</div>
			      		</div>
			      	</div> -->
			    </div>
			    <!-- Left and right controls -->
			    <a class="left carousel-control v-align" href="#happy-clients" data-slide="prev">
			      <img src="http://159.89.156.150/wp-content/uploads/2018/04/left_arrow.png" class="mw-100 responsive-img">
			      <span class="sr-only">Previous</span>
			    </a>
			    <a class="right carousel-control v-align" href="#happy-clients" data-slide="next">
			      <img src="http://159.89.156.150/wp-content/uploads/2018/04/right_arrow.png" class="mw-100 responsive-img">
			      <span class="sr-only">Next</span>
			    </a>
			</div>
		</div>
	</div>
	<div id="feature-footer">
		<div>						
			<div class="row clearfix-row">							
				<div class="col-md-12 col-sm-12 col-xs-12 clearfix-col">								
					<div class="feature-footer">									
						<div>										
							<p class="deliver mb-30">We deliver quick, customizable and user-centric Field Sales Productivity!</p>	
							<p class="mb-30 want">Want to know more? Get in touch directly!</p>										
							<div>
								<input type="email" placeholder="Email address" class="email"><button class="submit" onclick="sendMail();">Send</button>
							</div>	
							<div class='userMessage'>
							</div>								
						</div>								
					</div>							
				</div>						
			</div>					  
		</div>
	</div>
</section>
<?php get_footer() ?>
