<?php  extract(qode_get_blog_single_params()); ?>
<?php get_header(); ?>
<link rel="stylesheet" href="../../../../wp-content/themes/bridge/css/blog-details.css">
<link rel="stylesheet" href="../../../../wp-content/themes/bridge/css/bootstrap-min.css">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
		<script>
		var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
		</script>
	<?php } ?>
	<?php get_template_part( 'title' ); ?>
	<?php get_template_part( 'slider' ); ?>
				<?php if($single_type == 'image-title-post') : //this post type is full width ?>
					<div class="full_width" <?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
						<?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
							<div class="overlapping_content"><div class="overlapping_content_inner">
						<?php } ?>
						<div class="full_width_inner" <?php qode_inline_style($content_style_spacing); ?>>
				<?php else : // post type ?>
					<?php if ( has_post_thumbnail() ) : ?> 
							<div style="position: relative;">
								<div class="blog-details-hero" style="background: url('<?php the_post_thumbnail_url(); ?>') no-repeat;">
								</div>
								<div class="details-article-header">
									<p class="article-name">Articles  -  <?php the_time('F j, Y'); ?> </p>
									<h2 itemprop="name" class="entry_title artcle-heading"><?php the_title(); ?></h2>
								</div>
							</div>
						<?php endif; ?>
					<div class="container full-width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
						<?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
							<div class="overlapping_content"><div class="overlapping_content_inner">
						<?php } ?>
								<div class="container_inner default_template_holder" <?php qode_inline_style($content_style_spacing); ?>>
				<?php endif; // post type end ?>
					<?php if(($sidebar == "default")||($sidebar == "")) : ?>
						<div <?php qode_class_attribute(implode(' ', $single_class)) ?>>
						<?php 
							get_template_part('templates/' . $single_loop, 'loop');
						?>
						<?php if($single_grid == 'no'): ?>
							<div class="grid_section">
								<div class="section_inner">
						<?php endif; ?>
							<?php
								if($blog_hide_comments != "yes"){
									comments_template('', true);
								}else{
									echo "<br/><br/>";
								}
							?>
						<?php if($single_grid == 'no'): ?>
								</div>
							</div>
						<?php endif; ?>
                        </div>

                    <?php elseif($sidebar == "1" || $sidebar == "2"): ?>
						<?php if($sidebar == "1") : ?>	
							<div class="two_columns_66_33 background_color_sidebar grid2 clearfix">
							<div class="column1">
						<?php elseif($sidebar == "2") : ?>	
							<div class="two_columns_75_25 background_color_sidebar grid2 clearfix">
								<div class="column1">
						<?php endif; ?>
					
									<div class="column_inner">
										<div <?php qode_class_attribute(implode(' ', $single_class)) ?>>
											<?php
											get_template_part('templates/' . $single_loop, 'loop');
											?>
										</div>
										
										<?php
											if($blog_hide_comments != "yes"){
												comments_template('', true); 
											}else{
												echo "<br/><br/>";
											}
										?> 
									</div>
								</div>	
								<div class="column2"> 
									<?php get_sidebar(); ?>
								</div>
							</div>
						<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
							<?php if($sidebar == "3") : ?>	
								<div class="two_columns_33_66 background_color_sidebar grid2 clearfix">
								<div class="column1"> 
									<?php get_sidebar(); ?>
								</div>
								<div class="column2">
							<?php elseif($sidebar == "4") : ?>	
								<div class="two_columns_25_75 background_color_sidebar grid2 clearfix">
									<div class="column1"> 
										<?php get_sidebar(); ?>
									</div>
									<div class="column2">
							<?php endif; ?>
							
										<div class="column_inner">
											<div <?php qode_class_attribute(implode(' ', $single_class)) ?>>
												<?php
												get_template_part('templates/' . $single_loop, 'loop');
												?>
											</div>
											<?php
												if($blog_hide_comments != "yes"){
													comments_template('', true); 
												}else{
													echo "<br/><br/>";
												}
											?> 
										</div>
									</div>	
									
								</div>
						<?php endif; ?>
					</div>
                <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                    </div></div>
                <?php } ?>
                 </div>
<?php endwhile; ?>
<?php endif; ?>	
<script type="text/javascript" src="../../../../wp-content/themes/bridge/js/bootstrap.min.js">
</script>
<script src="https://cdn.jsdelivr.net/npm/sharer.js@latest/sharer.min.js"></script>
<script type="text/javascript" src="../../../../wp-content/themes/bridge/js/mustache.js">
</script>
<script type="text/javascript" src="../../../../wp-content/themes/bridge/js/footer.js">
</script>
<script type="text/javascript" src="../../../../wp-content/themes/bridge/js/sendEmail.js"></script>
<script type="text/javascript">
renderFooter();
jQuery(".header_inner_right").append('<div class="demo-desk"><button>Request a Demo</button></div>');
jQuery('.demo-desk').click(function(){
	window.location.href = '/#contact';
});
function openBlogHome() {
	window.location.href = '/learning-and-blog';
}
</script>
<?php get_footer(); ?>	