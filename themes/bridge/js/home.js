function setHomeData(){
	jQuery('.carousel .carousel-inner').css('display','block');
	jQuery('.carousel .carousel-indicators').css('display','block');
	if (jQuery('nav.main_menu').hasClass('left')) {
		jQuery(".main_menu").removeClass('left');
		jQuery(".main_menu").addClass('right');
		jQuery('.main_menu').css({'right':'203px'});
	}
	jQuery('.main_menu.second').css('height','356px');
	jQuery('header').addClass('home-header');	
	jQuery(".q_logo a .normal").attr({'src':'http://159.89.156.150/wp-content/uploads/2018/04/gc_logo.png'});
	jQuery(".q_logo a .sticky").attr({'src':'http://159.89.156.150/wp-content/uploads/2018/04/gc_logo.png'});	
	jQuery(".q_logo a .normal,.q_logo a .sticky").addClass('mh-55');
	jQuery(".main_menu").addClass('pt');
	jQuery(".main_menu span").addClass('header-features');
	jQuery(".header_bottom").attr('style','background-color: transparent !important');
	jQuery(".header_bottom").css({'padding-right':'67px','padding-left':'50px'});
	jQuery("#menu-main-menu li").append('<div class="home-header-line"></div>')
	jQuery(".header_inner_right").append('<div class="demo-desk home"><button>Request a Demo</button></div>');
	jQuery('.demo-desk').click(function(){
		window.location.href = '#contact';
	});
	jQuery('.header_inner_right .demo-desk').addClass('t-30');
	jQuery('.header_inner_right .demo-desk button').addClass('req-bt');
	jQuery("footer").addClass("home-footer");
}


// for youtube carousel
var $owl = jQuery('.owl-carousel');

$owl.children().each( function( index ) {
  jQuery(this).attr( 'data-position', index ); // NB: .attr() instead of .data()
});

$owl.owlCarousel({
  center: true,
  loop: true,
  items: 3,
  video: true,
  responsiveClass: true,
  nav: true,
  navText: ["<span class='learning-arrows left-arrow-bg'><img src='http://159.89.156.150/wp-content/uploads/2018/04/left_arrow.png'></span>","<span class='learning-arrows right-arrow-bg'><img src='http://159.89.156.150/wp-content/uploads/2018/04/right_arrow.png'></span>"],
  responsive:{
  	0:{
        items: 1
    },
    769:{
    	items: 3
    }
  }	
});

// on click on item change its position then play video
jQuery(document).on('click', '.owl-item>div', function() {
	// on click item change the position
  	$owl.trigger('to.owl.carousel', jQuery(this).data('position'));
  	var data_pos = jQuery(this).data('position');
  	jQuery('.youtube-player').each(function(){
  		// if it is active element then play the video, else show image
  		if(jQuery(this).data('position') == data_pos) {
  			var youtube_id = jQuery(this).data('id');
			var iframe = document.createElement("iframe");
		    iframe.setAttribute("src", "https://www.youtube.com/embed/" + youtube_id + '?autoplay=1&amp;rel=0&amp;fs=0&amp;showinfo=0');
		    iframe.setAttribute("frameborder", "0");
		    iframe.setAttribute("allowfullscreen", "allowFullScreen");
		    jQuery(this).children().html(iframe);
		    jQuery(this).addClass('iframe-exist');
  		} else {
  			var youtube_id = jQuery(this).data('id');
  			var div;
  			var div = '<div><img src="https://i.ytimg.com/vi/' + youtube_id + '/maxresdefault.jpg"><div class="play"></div></div>'
		    jQuery(this).children().html(div);
		    jQuery(this).removeClass('iframe-exist');
  		}
  	});
});
// load youtube videos only on click
document.addEventListener("DOMContentLoaded",function() {
    var div, n,
        v = document.getElementsByClassName("youtube-player");
    for (n = 0; n < v.length; n++) {
        div = document.createElement("div");
        div.setAttribute("data-id", v[n].dataset.id);
        div.innerHTML = labnolThumb(v[n].dataset.id);
        // div.onclick = labnolIframe;
        v[n].appendChild(div);
    }
});

function labnolThumb(id) {
	var thumb = '<img src="https://i.ytimg.com/vi/ID/maxresdefault.jpg">',
    play = '<div class="play"></div>';
    return thumb.replace("ID", id) + play;
}
jQuery('.bots').click(function(){
	window.location.href = '#contact';
});

jQuery(document).ready(function(){
	setHomeData();
  setMobileMenu();
})