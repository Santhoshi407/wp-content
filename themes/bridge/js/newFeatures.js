var result;
var url;
$(document).ready(function(){
	$.getJSON('../wp-content/themes/bridge/js/newFeatures.json',function(data) {
	    result = data;
        url = document.URL;
        if (url) {
        	url = url.split('/')[3];
        }
        getFeaturesData(url);
	}); 
   
});


function getFeaturesData(pageId){
	var features = result.features;
	for (var i=0;i<features.length;i++) {
		if (features[i].page == url) {
			if (url == 'lead-scoring-and-allocation') {
				$("#" + pageId).html(Mustache.render(features_template1,result.features[i]));
			} else {
				$("#" + pageId).html(Mustache.render(features_template,result.features[i]));
			}
			
		}
	}
}

function showSubFeature (id) {
	window.location.href = "#" + id;
}

var features_template = '<nav class="navbar navbar-default feature-nav h-60" style="margin-top:100px;">\
						  <div class="container-fluid">\
						    <ul class="nav navbar-nav feature-ul h-60">\
						      <li class="h-60 {{#isLead}}{{active}}{{/isLead}}"><a href="/wordpress/lead-management" class="text-center p-10-30">Lead <br/> Management</a></li>\
						      <li class="h-60 {{#isAllocation}}{{active}}{{/isAllocation}}"><a href="/wordpress/lead-scoring-and-allocation" class="text-center p-10-30">Lead Scoring <br/> and Allocation</a></li>\
						      <li class="h-60 {{#isPerformance}}{{active}}{{/isPerformance}}"><a href="/wordpress/performance-management" class="text-center p-10-30">Performance <br/> Management</a></li>\
						      <li class="h-60 {{#isGamification}}{{active}}{{/isGamification}}"><a href="/wordpress/gamification" class="text-center p-10-30">Gamification</a></li>\
						      <li class="h-60 {{#isRecommend}}{{active}}{{/isRecommend}}"><a href="/wordpress/recommendation-engine" class="text-center p-10-30">Recommendation <br/> Engine</a></li>\
						      <li class="h-60 {{#isChannel}}{{active}}{{/isChannel}}"><a href="/wordpress/social-channel" class="text-center p-10-30">Social <br/> Channel</a></li>\
						      <li class="h-60 {{#isSelling}}{{active}}{{/isSelling}}"><a href="/wordpress/social-selling" class="text-center p-10-30">Social <br/> Selling</a></li>\
						      <li class="h-60 {{#isGeo}}{{active}}{{/isGeo}}"><a href="/wordpress/geo-tagging" class="text-center p-10-30">Geo-tagging</a></li>\
						      <li class="h-60 {{#islearning}}{{active}}{{/islearning}}"><a href="/wordpress/frontline-learning" class="text-center p-10-30">Frontline <br/> Learning</a></li>\
						    </ul>\
						  </div>\
						</nav>\
						<section>\
							<div class="container">\
								<div class="row mt-20">\
									<div class="col-md-12 col-xs-12 col-sm-12">\
										<h2 class="tc">{{heading}}</h2>\
									</div>\
								</div>\
								<div class="row mt-20">\
									<div class="col-md-12 col-xs-12 col-sm-12 v-align center">\
										{{#subFeatures}}<div class="feature-bubbles">\
											<span class="w-100 p-10" onclick="showSubFeature(\'{{id}}\')">{{subFeature}}</span>\
										</div>{{/subFeatures}}\
									</div>\
								</div>\
								<div>\
								</div>\
								{{#subFeatures}}\
								<div class="row v-align mt-40" id="{{id}}">\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs">\
										{{#odd}}<div>\
											<h3>{{subFeature}}</h3>\
											&nbsp;\
											<ul class="features-9">\
											 	{{#subFeatureList}}<li>{{list}}</li>{{/subFeatureList}}\
											</ul>\
										</div>{{/odd}}\
										{{^odd}}<div>\
											<img src="{{img}}" class="mw-100" style="border-radius:10px;">\
										</div>{{/odd}}\
									</div>\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs">\
										{{^odd}}<div>\
											<h3>{{subFeature}}</h3>\
											&nbsp;\
											<ul class="features-9">\
											 	{{#subFeatureList}}<li>{{list}}</li>{{/subFeatureList}}\
											</ul>\
										</div>{{/odd}}\
										{{#odd}}<div>\
											<img src="{{img}}" class="mw-100">\
										</div>{{/odd}}\
									</div>\
									<div class="col-xs-12 hidden-sm hidden-md hidden-lg">\
										<div>\
											<h3>{{subFeature}}</h3>\
											&nbsp;\
											<ul class="features-9">\
											 	{{#subFeatureList}}<li>{{list}}</li>{{/subFeatureList}}\
											</ul>\
										</div>\
										<div>\
											<img src="{{img}}" class="mw-100">\
										</div>\
									</div>\
								</div>\
								{{/subFeatures}}\
							</div>\
							<div class="row">\
								<div class="col-md-12 col-sm-12 col-xs-12">\
									<div class="feature-footer">\
										<div>\
											<h3>We deliver quick, customizable and user-centric Field Sales Productivity!</h3>\
											<p>Want to know more? Get in touch directly!</p>\
											<div><input type="email" placeholder="your email address" /></div>\
											<div class="pos-rel"><input type="submit" value="SUBMIT" class="tl submit"></div>\
											<p>Dont worry, we hate spam &amp; love privacy as much as you do.</p>\
										</div>\
									</div>\
								</div>\
							</div>\
						</section>'
var features_template1 = '<nav class="navbar navbar-default feature-scoring-nav h-30" style="margin-top:100px;">\
						  <div class="container-fluid">\
						    <ul class="nav navbar-nav feature-ul h-40">\
						    	<li class="h-40"><a class="text-center p-10-15" style="font-size: 30px;">Features</a></li>\
						      <li class="h-40 {{#isLead}}{{active}}{{/isLead}}"><a href="/wordpress/lead-management" class="text-center p-10-15">Lead Management</a></li>\
						      <li class="h-40 {{#isAllocation}}{{active}}{{/isAllocation}}"><a href="/wordpress/lead-scoring-and-allocation" class="text-center p-10-15">Lead Scoring and Allocation</a></li>\
						      <li class="h-40 {{#isPerformance}}{{active}}{{/isPerformance}}"><a href="/wordpress/performance-management" class="text-center p-10-15">Performance Management</a></li>\
						      <li class="h-40 {{#isGamification}}{{active}}{{/isGamification}}"><a href="/wordpress/gamification" class="text-center p-10-15">Gamification</a></li>\
						      <li class="h-40 dropdown"><a class="text-center p-10-15 show-rem dropbtn"><i class="fa fa-ellipsis-h"></i></a>\
						      <div class="dropdown-content">\
							   	<a href="/wordpress/gamification">Gamification</a>\
							    <a href="/wordpress/recommendation-engine">Recommendation Engine</a>\
							    <a href="/wordpress/social-channel">Social Channel</a>\
							    <a href="/wordpress/social-selling">Social Selling</a>\
							    <a href="/wordpress/geo-tagging">Geo-tagging</a>\
							    <a href="/wordpress/frontline-learning">Frontline Learning</a>\
							  </div>\
							  </li>\
						    </ul>\
						  </div>\
						</nav>\
						<section>\
							<div class="container-fluid">\
								<div class="row mt-20">\
									<div class="col-md-12 col-xs-12 col-sm-12">\
										<h2 class="tc">{{heading}}</h2>\
									</div>\
								</div>\
								<div class="row mt-20">\
									<div class="col-md-12 col-xs-12 col-sm-12 v-align center">\
										{{#subFeatures}}<div class="feature-bubbles">\
											<span class="w-100 p-10" onclick="showSubFeature(\'{{id}}\')">{{subFeature}}</span>\
										</div>{{/subFeatures}}\
									</div>\
								</div>\
								<div>\
								</div>\
								{{#subFeatures}}\
								<div class="row v-align mt-40 {{#odd}}{{white}}{{/odd}} {{^odd}}{{white}}{{/odd}}" id="{{id}}">\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs">\
										{{#odd}}<div>\
											<h3>{{subFeature}}</h3>\
											&nbsp;\
											<ul class="features-9">\
											 	{{#subFeatureList}}<li>{{list}}</li>{{/subFeatureList}}\
											</ul>\
										</div>{{/odd}}\
										{{^odd}}<div>\
											<img src="{{img}}" class="mw-70" style="border-radius:10px;">\
										</div>{{/odd}}\
									</div>\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs">\
										{{^odd}}<div>\
											<h3>{{subFeature}}</h3>\
											&nbsp;\
											<ul class="features-9">\
											 	{{#subFeatureList}}<li>{{list}}</li>{{/subFeatureList}}\
											</ul>\
										</div>{{/odd}}\
										{{#odd}}<div>\
											<img src="{{img}}" class="mw-70">\
										</div>{{/odd}}\
									</div>\
									<div class="col-xs-12 hidden-sm hidden-md hidden-lg">\
										<div>\
											<h3>{{subFeature}}</h3>\
											&nbsp;\
											<ul class="features-9">\
											 	{{#subFeatureList}}<li>{{list}}</li>{{/subFeatureList}}\
											</ul>\
										</div>\
										<div>\
											<img src="{{img}}" class="mw-70">\
										</div>\
									</div>\
								</div>\
								{{/subFeatures}}\
							</div>\
							<div class="row">\
								<div class="col-md-12 col-sm-12 col-xs-12">\
									<div class="feature-footer-lead">\
										<div>\
											<h3>Learn what BitNudge can do to accelerate your frontline\'s performance and capability building</h3>\
											<div class="btn btn-primary btn-sm">\
												<p>REQUEST DEMO</p>\
											</div>\
										</div>\
									</div>\
								</div>\
							</div>\
						</section>'
