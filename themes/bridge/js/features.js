var result;
var url;
jQuery(document).ready(function(){
	jQuery.getJSON('../wp-content/themes/bridge/js/features.json',function(data) {
	    result = data;
        url = document.URL;
        if (url) {
        	url = url.split('/')[3];
        }
        getFeaturesData(url);
	}); 
	jQuery("#performance-management #feature-nav").mouseover(function(){
		alert("itno");
	})
});


function showDropdown() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

function getFeaturesData(pageId){
	var features = result.features;
	for (var i=0;i<features.length;i++) {
		if (features[i].page == url) {
			var subFeaturesLength, imageUrl;
			jQuery("#" + pageId).html(Mustache.render(features_template,result.features[i]));
			jQuery("#feature-nav").html(Mustache.render(feature_nav,result.features[i]));
			jQuery("#feature-desc").html(Mustache.render(feature_desc,result.features[i]));
			jQuery("#feature-main-image").html(Mustache.render(feature_main_image,result.features[i]));
			jQuery("#sub-feature-template").html(Mustache.render(sub_features_template,result.features[i]));
			renderFooter();
			subFeaturesLength = jQuery('.sub-features').length;
			if (url == 'lead-scoring-and-allocation') {
				jQuery('.background-image').addClass('background-mob');
			} else {
				jQuery('.background-image').addClass('background-auto');
			}
			if (subFeaturesLength < 3) {
				jQuery('.subfeature-header').css('grid-template-columns','repeat(2, 1fr)');
			} else {
				jQuery('.subfeature-header').css('grid-template-columns','repeat(3, 1fr)');
			}
			imageUrl = result.features[i].mainImage;
			jQuery("#feature-main-image").css('background-image','url(' + imageUrl + ')');
			if (url == 'lead-management' || url == 'social-selling' || url == 'frontline-learning' || url == 'geo-tagging' || url == 'social-channel' || url == 'lead-scoring-and-allocation' || url == 'recommendation-engine' || url == 'performance-management') {
				jQuery("#feature-main-image").addClass('h-760');
			}else {
				jQuery("#feature-main-image").addClass('h-800');
			}
			if (url == 'lead-management'){
				jQuery(".sub-feature .sub-features").addClass("mlr-53");
			} else if (url == 'frontline-learning' || url == 'lead-scoring-and-allocation'){
				jQuery(".sub-feature .sub-features").addClass("mlr-133");
				jQuery(".sub-feature .sub-features span").css("margin","0 auto");
			} else if (url == 'geo-tagging' || url == 'social-channel') {
				jQuery(".sub-feature .sub-features").addClass("mlr-103");
				jQuery(".sub-feature .sub-features span").css("margin","0 auto");
			} else if (url == 'social-selling') {
				jQuery(".sub-feature .sub-features").addClass("mlr-123");
			} else if (url == 'recommendation-engine') {
				jQuery(".sub-feature .sub-features").addClass("mlr-63");
				jQuery(".sub-feature .sub-features span").css("margin","0 auto");
			} else if (url == 'performance-management') {
				jQuery(".sub-feature .sub-features").addClass("per-mlr");
			}
			if (url == "frontline-learning") {
				jQuery("#feature-desc p").css('margin','110px auto');
			}
			jQuery(".active-feature").text(url);
			if (url == 'social-selling'){
				jQuery("#do .features-9 .webkit-inline li").append('<span style="visibility:hidden;">Customers</span>');
			}
		}
	}
	jQuery(".header_inner_right").append('<div class="demo-desk"><button>Request a Demo</button></div>');
	jQuery('.demo-desk').click(function(){
		window.location.href = '/#contact';
	});
}

function showSubFeature (id) {
	window.location = "#" + id;
}

// replace gray image to color for banks on hover
function changeToColorImage(elem) {
	// console.log(elem);
	// console.log(jQuery(this));
	// console.log(image);
	// var element = document

}




var features_template = '<div>\
							<div id="feature-nav">\
							</div>\
							<div id="feature-main-image" class="background-image">\
							</div>\
							<div id="feature-desc">\
							</div>\
							<div id="sub-feature-template">\
							</div>\
							<div id="common-footer" class="background-white">\
							</div>\
							<div id="feature-footer">\
							</div>\
						</div>'

var feature_nav = '<nav class="navbar navbar-default feature-nav v-align hidden-xs hidden-sm hide-lg hidden-md" style="margin-top:80px;">\
					  <div class="container-fluid clearfix-col h-45">\
					    <ul class="nav navbar-nav feature-ul h-45 v-align">\
					      <li class="h-45 {{#isLead}}selected{{/isLead}}"><a href="/wordpress/lead-management" class="text-center">LEAD MANAGEMENT</a></li>\
					      <li class="h-45 {{#isAllocation}}selected{{/isAllocation}}"><a href="/wordpress/lead-scoring-and-allocation" class="text-center">LEAD SCORING & ALLOCATION</a></li>\
					      <li class="h-45 {{#isPerformance}}selected{{/isPerformance}}"><a href="/wordpress/performance-management" class="text-center">PERFORMANCE MANAGEMENT</a></li>\
					      <li class="h-45 {{#isGamification}}selected{{/isGamification}}"><a href="/wordpress/gamification" class="text-center">GAMIFICATION</a></li>\
					      <li class="h-45 {{#isRecommend}}selected{{/isRecommend}}"><a href="/wordpress/recommendation-engine" class="text-center">RECOMMENDATION ENGINE</a></li>\
					      <li class="h-45 {{#isChannel}}selected{{/isChannel}}"><a href="/wordpress/social-channel" class="text-center">SOCIAL CHANNEL</a></li>\
					      <li class="h-45 {{#isSelling}}selected{{/isSelling}}"><a href="/wordpress/social-selling" class="text-center">SOCIAL SELLING</a></li>\
					      <li class="h-45 {{#isGeo}}selected{{/isGeo}}"><a href="/wordpress/geo-tagging" class="text-center">GEO-TAGGING</a></li>\
					      <li class="h-45 {{#islearning}}selected{{/islearning}}"><a href="/wordpress/frontline-learning" class="text-center">FRONTLINE LEARNING</a></li>\
					    </ul>\
					  </div>\
					</nav>\
					<nav class="navbar navbar-default feature-nav v-align hidden-xs hidden-xl" style="margin-top:80px;">\
					  <div class="container-fluid clearfix-col h-50">\
					    <ul class="nav navbar-nav feature-ul h-50 v-align">\
					      <li class="h-50 {{#isLead}}selected{{/isLead}}"><a href="/wordpress/lead-management" class="text-center">LEAD <br/> MANAGEMENT</a></li>\
					      <li class="h-50 {{#isAllocation}}selected{{/isAllocation}}"><a href="/wordpress/lead-scoring-and-allocation" class="text-center">LEAD SCORING <br/> & ALLOCATION</a></li>\
					      <li class="h-50 {{#isPerformance}}selected{{/isPerformance}}"><a href="/wordpress/performance-management" class="text-center">PERFORMANCE <br/> MANAGEMENT</a></li>\
					      <li class="h-50 {{#isGamification}}selected{{/isGamification}}"><a href="/wordpress/gamification" class="text-center">GAMIFICATION</a></li>\
					      <li class="h-50 {{#isRecommend}}selected{{/isRecommend}}"><a href="/wordpress/recommendation-engine" class="text-center">RECOMMENDATION <br/> ENGINE</a></li>\
					      <li class="h-50 {{#isChannel}}selected{{/isChannel}}"><a href="/wordpress/social-channel" class="text-center">SOCIAL <br/> CHANNEL</a></li>\
					      <li class="h-50 {{#isSelling}}selected{{/isSelling}}"><a href="/wordpress/social-selling" class="text-center">SOCIAL <br/> SELLING</a></li>\
					      <li class="h-50 {{#isGeo}}selected{{/isGeo}}"><a href="/wordpress/geo-tagging" class="text-center">GEO-TAGGING</a></li>\
					      <li class="h-50 {{#islearning}}selected{{/islearning}}"><a href="/wordpress/frontline-learning" class="text-center">FRONTLINE <br/> LEARNING</a></li>\
					    </ul>\
					  </div>\
					</nav>\
					<nav class="navbar navbar-default feature-nav flex v-align hidden-lg hidden-sm hidden-md hidden-xl nav-hide">\
					  <div class="container-fluid clearfix-col">\
					    <ul class="nav navbar-nav feature-ul flex v-align">\
					    <li><a class="text-left"><span class="features-mob">Features: </span> <span class="active-feature">LEAD MANAGEMENT</span> <span onclick="showDropdown()" class="dropbtn glyphicon glyphicon-triangle-bottom"></span></a></li>\
					   	<div class="dropdown">\
						  	<div id="myDropdown" class="dropdown-content">\
						  		<a href="/wordpress/lead-management" class="text-center">LEAD MANAGEMENT</a>\
						    	<a href="/wordpress/lead-scoring-and-allocation" class="text-center">LEAD SCORING & ALLOCATION</a>\
						    	<a href="/wordpress/performance-management" class="text-center">PERFORMANCE MANAGEMENT</a>\
						    	<a href="/wordpress/gamification" class="text-center">GAMIFICATION</a>\
						    	<a href="/wordpress/recommendation-engine" class="text-center">RECOMMENDATION ENGINE</a>\
						    	<a href="/wordpress/social-channel" class="text-center">SOCIAL CHANNEL</a>\
						    	<a href="/wordpress/social-selling" class="text-center">SOCIAL SELLING</a>\
						    	<a href="/wordpress/geo-tagging" class="text-center">GEO-TAGGING</a>\
						    	<a href="/wordpress/frontline-learning" class="text-center">FRONTLINE LEARNING</a>\
						  	</div>\
						</div>\
					    </ul>\
					  </div>\
					</nav>';

var feature_desc = '{{#desc}}<div class="background-white">\
						<div class="row clearfix-row v-align m-0 background-white">\
							<div class="col-md-4 col-sm-4 col-lg-4 col-xs-4 clearfix-col m-desc p-0">\
								{{#odd}}<div class="">\
									<div>\
										<img src="{{feature-desc}}" class="mw-100">\
									</div>\
								</div>{{/odd}}\
								{{^odd}}<div>\
									<p class="clearfix-p subfeature title-main black mtb-30">{{title}}</p>\
									<p class="content-style">{{{content}}}</p>\
								</div>{{/odd}}\
							</div>\
							<div class="col-md-8 col-sm-8 col-lg-8 col-xs-8 clearfix-col">\
								{{#odd}}<div class="{{#odd}}ml-90 mr-190{{/odd}}">\
									<div>\
										<p class="clearfix-p subfeature title-main black mtb-30">{{title}}</p>\
										<p class="content-style">{{{content}}}</p>\
									</div>\
								</div>{{/odd}}\
								{{^odd}}<div>\
										<img src="{{feature-desc}}" class="mw-100">\
								</div>{{/odd}}\
							</div>\
						</div>\
					</div>{{/desc}}\
					{{#desc-img}}<div class="show-background">\
						<div class="text-center w-60 m-a">\
							<p class="clearfix-p">The success of any platform is as good as its adoption. Majority of sales and performance acceleration platforms fail dramatically after implementation — primarily due to poor user adoption. User adoption is the oxygen that adds life to a Platform and absence of which can bring early extinction to even the most functional of the platforms.</p>\
							<p class="clearfix-p">When embedded carefully into a Platform, Gamification can address the key bottlenecks and mindsets that can limit user adoption.</p>\
							<p class="clearfix-p">Most importantly, Gamification brings an answer to the key question that every user asks —"why should I use this platform". The answer to the question goes beyond functional reasons into emotional factors (motivation) — which is precisely the dimension that Gamification engine within BitNudge addresses.</p>\
						</div>\
					</div>{{/desc-img}}\
					{{#desc-text}}<p class="desc-text {{#lh-48}}lh-48{{/lh-48}} {{#isSelling}}m-150-auto{{/isSelling}} {{#m-150-60-auto}}m-150-60-auto{{/m-150-60-auto}} {{^show-margin}}m-200-auto{{/show-margin}} {{#w-49}}w-49{{/w-49}} {{#gamification-desc}}gamification-text{{/gamification-desc}} {{#show-margin}}m-50-auto{{/show-margin}} w-60 clearfix-p">{{{description}}}</p>{{/desc-text}}';

var feature_main_image = '<div class="{{#show}}sub-pos{{/show}} hidden-xs hidden-xl">\
							<div class="row clearfix-row">\
								<div class="col-md-12 col-xs-12 col-sm-12 clearfix-col">\
									{{#heading}}<p class="tc heading mt-62 heading-text {{#isPerformance}}heading-perfor{{/isPerformance}} {{heading-1920}} {{#font-46}}font-46{{/font-46}}">{{&heading}}</p>{{/heading}}\
									{{#heading1}}<p class="tc clearfix-p heading mt-62 heading-text {{heading-1920}}">{{heading1}}</p>{{/heading1}}\
									{{#heading2}}<p class="tc clearfix-p heading heading-text heading2">{{heading2}}</p>{{/heading2}}\
									<p class="tc clearfix-p m-0 subheading black {{#isPerformance}}subheading-per{{/isPerformance}} {{#isRecommend}}recommend{{/isRecommend}}">{{sub-heading}}</p>\
								</div>\
							</div>\
							{{#show}}\
							<div class="row clearfix-row mb-53 text-center">\
								<div class="v-align text-center sub-feature">\
									{{#subFeatures}}{{^subfeature-exist}}<div class="mr-160 sub-features" onclick="showSubFeature(\'{{id}}\')">\
										<div class="mb-20">\
											<img src="{{subfeature-img}}" class="mw-100 responsive-img">\
										</div>\
										{{^subfeature-header}}<span class="w-100 black m-a {{#lh-10}}lh-10{{/lh-10}} {{#text-margin}}mr-50{{/text-margin}}">{{&subFeature}}</span>{{/subfeature-header}}\
										{{#subfeature-header}}<span class="w-100 black m-a {{#lh-10}}lh-10{{/lh-10}} {{#text-margin}}mr-50{{/text-margin}}">{{&subfeature-header}}</span>{{/subfeature-header}}\
									</div>{{/subfeature-exist}}{{/subFeatures}}\
								</div>\
							</div>\
							{{/show}}\
						</div>\
						<div class="{{#show}}sub-pos{{/show}} hidden-xs hidden-md hidden-sm hide-lg">\
							<div class="row clearfix-row">\
								<div class="col-md-12 col-xs-12 col-sm-12 clearfix-col">\
									{{#heading}}<p class="tc heading mt-62 heading-text {{#isPerformance}}heading-perfor{{/isPerformance}} {{heading-1920}} {{#font-46}}font-46{{/font-46}}">{{&heading}}</p>{{/heading}}\
									{{#heading1}}<p class="tc clearfix-p heading mt-62 heading-text {{heading-1920}}">{{heading1}}</p>{{/heading1}}\
									{{#heading2}}<p class="tc clearfix-p heading heading-text heading2">{{heading2}}</p>{{/heading2}}\
									<p class="tc clearfix-p m-0 subheading black {{#isPerformance}}subheading-per{{/isPerformance}} {{#isRecommend}}recommend{{/isRecommend}}">{{sub-heading}}</p>\
								</div>\
							</div>\
							{{#show}}\
							<div class="row clearfix-row mb-53 text-center">\
								<div class="v-align text-center sub-feature">\
									{{#subFeatures}}{{^subfeature-exist}}<div class="mr-160 sub-features" onclick="showSubFeature(\'{{id-1920}}\')">\
										<div class="mb-20">\
											<img src="{{subfeature-img}}" class="mw-100 responsive-img">\
										</div>\
										{{^subfeature-header}}<span class="w-100 black m-a {{#lh-10}}lh-10{{/lh-10}} {{#text-margin}}mr-50{{/text-margin}}">{{&subFeature}}</span>{{/subfeature-header}}\
										{{#subfeature-header}}<span class="w-100 black m-a {{#lh-10}}lh-10{{/lh-10}} {{#text-margin}}mr-50{{/text-margin}}">{{&subfeature-header}}</span>{{/subfeature-header}}\
									</div>{{/subfeature-exist}}{{/subFeatures}}\
								</div>\
							</div>\
							{{/show}}\
						</div>\
						<div class="hidden-sm hidden-md hidden-lg mob nav-hide {{#show}}sub-pos{{/show}}">\
							<div class="row clearfix-row">\
								<div class="col-xs-12 clearfix-col">\
									<p class="tc clearfix-p mtb-62-30 heading">{{heading}}</p>\
								</div>\
							</div>\
							{{#show}}\
							<div class="row clearfix-row">\
								<div class="clearfix-col subfeature-header text-center">\
									{{#subFeatures}}{{^subfeature-exist}}<div class="p-0 mb-10" onclick="showSubFeature(\'{{id-mob}}\')">\
										<div class="mb-10">\
											<img src="{{subfeature-img}}" class="mw-40p">\
										</div>\
										{{^subfeature-header}}<span class="w-100 black">{{subFeature}}</span>{{/subfeature-header}}\
										{{#subfeature-header}}<span class="w-100 black">{{&subfeature-header}}</span>{{/subfeature-header}}\
									</div>{{/subfeature-exist}}{{/subFeatures}}\
								</div>\
							</div>\
							{{/show}}\
						</div>';

var sub_features_template = '<div>\
							{{!css for desktop view}}\
							<div class="container-fluid clearfix-col hidden-xs hidden-xl">\
								{{#subFeatures}}\
								{{^show-subfeature}}<div class="row clearfix-row v-align {{background}} {{^FullImage}}h-461{{/FullImage}}" id="{{id}}">\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs clearfix-col">\
										{{#odd}}<div class="{{#odd}}ml-50 mr-80{{/odd}}">\
											<div class="subfeature-img">\
												<img src="{{subfeature-img}}" class="mw-100">\
											</div>\
											<p class="clearfix-p subfeature feature-title mtb-30">{{subFeature}}</p>\
											<ul class="features-9">\
											 	{{#subFeatureList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle inner-circle-pos"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subFeatureList}}\
											 	{{#scoring-graph}}<img src="{{lead-scoring}}" class="mw-100 graph-position">{{/scoring-graph}}\
											</ul>\
										</div>{{/odd}}\
										{{^odd}}<div>\
											<img src="{{img}}" class="mw-100">\
										</div>{{/odd}}\
									</div>\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs clearfix-col {{^FullImage}}mr-100{{/FullImage}} {{#media}}mr-0{{/media}}">\
										{{^odd}}<div class="{{^odd}}mr-100 mlr-50{{/odd}} {{#performance-desc}}mlr-per{{/performance-desc}} {{#planning}}mlr-50{{/planning}}">\
											<div class="subfeature-img">\
												<img src="{{subfeature-img}}" class="mw-100">\
											</div>\
											<p class="clearfix-p feature-title mtb-30">{{subFeature}}</p>\
											<ul class="features-9">\
											 	{{#subFeatureList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle {{#per-inner}}per-inner{{/per-inner}} inner-circle-pos"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subFeatureList}}\
											</ul>\
										</div>{{/odd}}\
										{{#odd}}<div>\
											<img src="{{img}}" class="mw-100 {{#media}}media{{/media}}">\
										</div>{{/odd}}\
									</div>\
								</div>{{/show-subfeature}}\
								{{/subFeatures}}\
							</div>\
							{{!css for large screen view >1600px}}\
							<div class="container-fluid clearfix-col hidden-xs hidden-sm hide-lg hidden-md">\
								{{#subFeatures}}\
								{{^show-subfeature}}<div class="row clearfix-row v-align {{background}} {{^FullImage-1920}}h-461{{/FullImage-1920}} {{#h-561}}h-680{{/h-561}}" id="{{id-1920}}">\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs clearfix-col">\
										{{#odd}}<div class="{{#odd}}ml-152 mtb-100 {{#mb-80}}mb-80{{/mb-80}} {{#mt-0}}mt0{{/mt-0}} {{#mtb-0}}mtb0{{/mtb-0}} mr-80 {{#mr-10}}mr-10{{/mr-10}}{{/odd}}">\
											{{#subfeature-img}}<div class="subfeature-img">\
												<img src="{{subfeature-img}}" class="mw-100">\
											</div>{{/subfeature-img}}\
											<p class="clearfix-p subfeature feature-title {{#w-82}}w-82{{/w-82}} mtb-30 {{#mt-0}}m0{{/mt-0}}">{{subFeature}}</p>\
											<ul class="features-9">\
											 	{{#subFeatureList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle inner-circle-pos"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subFeatureList}}\
											 	{{#scoring-graph}}<img src="{{lead-scoring}}" class="mw-200 graph-position">{{/scoring-graph}}\
											</ul>\
										</div>{{/odd}}\
										{{^odd}}<div class="{{#h-905}}h-905{{/h-905}} {{#h-670}}h-670{{/h-670}}">\
											<img src="{{img}}" class="mw-100 {{#near-real-time}}near-real-time{{/near-real-time}} {{#FullImage-1920}}w-960{{/FullImage-1920}} {{^FullImage-1920}}mw-70{{/FullImage-1920}}">\
										</div>{{/odd}}\
									</div>\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs clearfix-col {{^FullImage-1920}}mr-100{{/FullImage-1920}} {{#scoring-img}}text-right mr-0{{/scoring-img}} {{#media}}mr-0{{/media}}">\
										{{^odd}}<div class="{{^odd}}mr-200 mtb-100 {{#mb-80}}mb-80{{/mb-80}} {{#mt-0}}mt0{{/mt-0}} {{#mtb-0}}mtb0{{/mtb-0}} ml-90 mr-150{{/odd}}  {{#performance-desc}}mlr-per{{/performance-desc}} {{#planning}}mlr-50{{/planning}}">\
											{{#subfeature-img}}<div class="subfeature-img">\
												<img src="{{subfeature-img}}" class="mw-100">\
											</div>{{/subfeature-img}}\
											<p class="clearfix-p feature-title {{#w-82}}w-82{{/w-82}} mtb-30 {{#mt-0}}m0{{/mt-0}}">{{subFeature}}</p>\
											<ul class="features-9">\
											 	{{#subFeatureList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle {{#per-inner}}per-inner{{/per-inner}} inner-circle-pos-rt"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subFeatureList}}\
											</ul>\
										</div>{{/odd}}\
										{{#odd}}<div class="{{#h-905}}h-905{{/h-905}} {{#h-670}}h-670{{/h-670}}">\
											<img src="{{img}}" class="mw-100 {{#near-real-time}}near-real-time{{/near-real-time}} {{#FullImage-1920}}w-960{{/FullImage-1920}} {{^FullImage-1920}}mw-70{{/FullImage-1920}} {{#media}}media{{/media}}">\
										</div>{{/odd}}\
									</div>\
								</div>{{/show-subfeature}}\
								{{/subFeatures}}\
							</div>\
							{{!css for mobile view}}\
							<div class="container-fluid clearfix-col hidden-lg hidden-md hidden-sm hidden-xl nav-hide">\
								{{#subFeatures}}\
								{{^show-subfeature}}<div class="row clearfix-row v-align {{background}}" id="{{id-mob}}">\
									<div class="col-xs-12 hidden-sm hidden-md hidden-lg clearfix-col">\
										<div>\
											<p class="clearfix-p feature-title mtb-30 flex"><img src="{{subfeature-img}}" class="mw-40p mr-10">{{subFeature}}</p>\
											<ul class="features-9">\
											 	{{#subFeatureList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle {{#per-inner}}per-inner{{/per-inner}} inner-circle-pos-rt"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subFeatureList}}\
											</ul>\
										</div>\
										<div>\
											<img src="{{img}}" class="mw-100">\
										</div>\
									</div>\
								</div>{{/show-subfeature}}\
								{{/subFeatures}}\
							</div>\
						</div>';

