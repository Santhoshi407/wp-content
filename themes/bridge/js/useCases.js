var result;
var url;
jQuery(document).ready(function(){
	jQuery.getJSON('../wp-content/themes/bridge/js/useCases.json',function(data) {
		result = data;
        url = document.URL;
        if (url) {
        	url = url.split('/')[3];
        }
        getCasesData(url);
	}); 
});


function showDropdown() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

function getCasesData(pageId){
	var usecases = result.usecases;
	for (var i=0;i<usecases.length;i++) {
		if (usecases[i].page == url) {
			jQuery("#" + pageId).html(Mustache.render(cases_template,result.usecases[i]));
			jQuery("#case-nav").html(Mustache.render(case_nav,result.usecases[i]));
			jQuery("#case-desc").html(Mustache.render(case_desc,result.usecases[i]));
			jQuery("#case-main-image").html(Mustache.render(case_main_image,result.usecases[i]));
			jQuery("#sub-case-template").html(Mustache.render(sub_cases_template,result.usecases[i]));
			renderFooter();
			// show background images for cases
			var heroImage = usecases[i].mainImage;
			jQuery("#case-main-image").css('background-image','url(' + heroImage + ')');
			if (url == 'artificial-intelligence-bots') {
				jQuery(".sub-feature .sub-features").addClass("mlr-63");
			} else {
				jQuery(".sub-feature .sub-features").addClass("mlr-53");
			}
			jQuery(".active-feature").text(url);
		}
	}
	jQuery(".header_inner_right").append('<div class="demo-desk"><button>Request a Demo</button></div>');
	jQuery('.demo-desk').click(function(){
		window.location.href = '/#contact';
	});
}

function showSubCase (id) {
	window.location = "#" + id;
}





var cases_template = '<div>\
							<div id="case-nav">\
							</div>\
							<div id="case-main-image" class="background-image h-760">\
							</div>\
							<div id="case-desc">\
							</div>\
							<div id="sub-case-template">\
							</div>\
							<div id="common-footer" class="background-white">\
							</div>\
							<div id="feature-footer">\
							</div>\
						</div>'

var case_nav = '<nav class="navbar navbar-default feature-nav v-align hidden-xs hidden-sm hide-lg hidden-md" style="margin-top:80px;">\
					  <div class="container-fluid cases-nav clearfix-col h-45">\
					    <ul class="nav navbar-nav feature-ul h-45 v-align">\
					      <li class="h-45 {{#is-acceleration}}selected{{/is-acceleration}}"><a href="/wordpress/sales-acceleration" class="text-center">SALES ACCELERATION</a></li>\
					      <li class="h-45 {{#is-back-office}}selected{{/is-back-office}}"><a href="/wordpress/back-office-performance-management" class="text-center">BACK OFFICE PERFORMANCE MANAGEMENT</a></li>\
					      <li class="h-45 {{#is-feedback}}selected{{/is-feedback}}"><a href="/wordpress/customer-feedback-management" class="text-center">CUSTOMER FEEDBACK MANAGEMENT</a></li>\
					      <li class="h-45 {{#is-intelligence}}selected{{/is-intelligence}}"><a href="/wordpress/artificial-intelligence-bots" class="text-center">ARTIFICIAL INTELLIGENCE BOTS</a></li>\
					    </ul>\
					  </div>\
					</nav>\
					<nav class="navbar navbar-default feature-nav v-align hidden-xs hidden-xl" style="margin-top:80px;">\
					  <div class="container-fluid cases-nav clearfix-col h-50">\
					    <ul class="nav navbar-nav feature-ul h-50 v-align">\
					      <li class="h-45 {{#is-acceleration}}selected{{/is-acceleration}}"><a href="/wordpress/sales-acceleration" class="text-center">SALES ACCELERATION</a></li>\
					      <li class="h-45 {{#is-back-office}}selected{{/is-back-office}}"><a href="/wordpress/back-office-performance-management" class="text-center">BACK OFFICE PERFORMANCE MANAGEMENT</a></li>\
					      <li class="h-45 {{#is-feedback}}selected{{/is-feedback}}"><a href="/wordpress/customer-feedback-management" class="text-center">CUSTOMER FEEDBACK MANAGEMENT</a></li>\
					      <li class="h-45 {{#is-intelligence}}selected{{/is-intelligence}}"><a href="/wordpress/artificial-intelligence-bots" class="text-center">ARTIFICIAL INTELLIGENCE BOTS</a></li>\
					    </ul>\
					  </div>\
					</nav>\
					<nav class="navbar navbar-default feature-nav flex v-align hidden-lg hidden-sm hidden-md hidden-xl nav-hide">\
					  <div class="container-fluid cases-nav clearfix-col">\
					    <ul class="nav navbar-nav feature-ul flex v-align">\
					    <li><a class="text-left"><span class="features-mob">Features: </span> <span class="active-feature">SALES ACCELERATION</span> <span onclick="showDropdown()" class="dropbtn glyphicon glyphicon-triangle-bottom"></span></a></li>\
					   	<div class="dropdown">\
						  	<div id="myDropdown" class="dropdown-content">\
						  		<a href="/wordpress/sales-acceleration" class="text-center">SALES ACCELERATION</a>\
						    	<a href="/wordpress/back-office-performance-management" class="text-center">BACK OFFICE PERFORMANCE MANAGEMENT</a>\
						    	<a href="/wordpress/customer-feedback-management" class="text-center">CUSTOMER FEEDBACK MANAGEMENT</a>\
						    	<a href="/wordpress/artificial-intelligence-bots" class="text-center">ARTIFICIAL INTELLIGENCE BOTS</a>\
						  	</div>\
						</div>\
					    </ul>\
					  </div>\
					</nav>';

var case_desc = '{{#desc}}<div class="background-white">\
						<div class="row clearfix-row v-align m-0 background-white">\
							<div class="col-md-8 col-sm-8 col-lg-8 col-xs-6 clearfix-col p-0 ml-desc-text">\
								{{#odd}}<div class="">\
									<div>\
										<img src="{{feature-desc}}" class="mw-100">\
									</div>\
								</div>{{/odd}}\
								{{^odd}}<div>\
									<p class="clearfix-p subfeature title-main black mtb-30">{{title}}</p>\
									<ul class="usecases-list">\
										{{#content}}<li>{{&content-list}}</li>{{/content}}\
									</ul>\
								</div>{{/odd}}\
							</div>\
							<div class="col-md-4 col-sm-4 col-lg-4 col-xs-6 clearfix-col mr-172">\
								{{#odd}}<div class="{{#odd}}ml-90 mr-190{{/odd}}">\
									<div>\
										<p class="clearfix-p subfeature title-main black mtb-30"></p>\
										<p class="content-style">{{{content}}}</p>\
									</div>\
								</div>{{/odd}}\
								{{^odd}}<div>\
										<img src="{{feature-desc}}" class="mw-100">\
								</div>{{/odd}}\
							</div>\
						</div>\
					</div>{{/desc}}\
					{{#desc-img}}<div class="show-background">\
						<div class="text-center w-60 m-a">\
							<p class="clearfix-p">The success of any platform is as good as its adoption. Majority of sales and performance acceleration platforms fail dramatically after implementation — primarily due to poor user adoption. User adoption is the oxygen that adds life to a Platform and absence of which can bring early extinction to even the most functional of the platforms.</p>\
							<p class="clearfix-p">When embedded carefully into a Platform, Gamification can address the key bottlenecks and mindsets that can limit user adoption.</p>\
							<p class="clearfix-p">Most importantly, Gamification brings answer to the key question that every user asks —"why should I use this platform". The answer to the question goes beyond functional reasons into emotional factors (motivation) — which is precisely the dimension that Gamification engine within BitNudge addresses.</p>\
						</div>\
					</div>{{/desc-img}}\
					{{#desc-text}}<p class="desc-text {{^show-margin}}m-200-auto{{/show-margin}} {{#show-margin}}m-50-auto{{/show-margin}} {{desc-width}} clearfix-p">{{{description}}}</p>{{/desc-text}}\
					{{#desc1}}<p class="desc1 customer-desc text-center clearfix-p">{{{desc1}}}</p>{{/desc1}}\
					{{#desc2}}<p class="desc2 customer-desc text-center clearfix-p">{{{desc2}}}</p>{{/desc2}}';


var case_main_image = '	<div class="{{#show}}sub-pos{{/show}} hidden-xs hidden-xl">\
							<div class="row clearfix-row">\
								<div class="col-md-12 col-xs-12 col-sm-12 clearfix-col">\
									{{#heading}}<p class="tc heading mt-62 heading-text {{heading-case}}">{{&heading}}</p>{{/heading}}\
									<p class="tc clearfix-p m-0 subheading black {{#isPerformance}}subheading-per{{/isPerformance}} {{#isRecommend}}recommend{{/isRecommend}}">{{sub-heading}}</p>\
								</div>\
							</div>\
							{{#show}}\
							<div class="row clearfix-row mb-53 text-center">\
								<div class="v-align text-center sub-feature">\
									{{#subCases}}{{^hide-subfeature-header}}<div class="mr-160 sub-features" onclick="showSubCase(\'{{id}}\')">\
										<div class="mb-20">\
											<img src="{{subCase-img}}" class="mw-100 responsive-img">\
										</div>\
										{{^subCase-header}}<span class="w-100 black m-a {{#lh-10}}lh-10{{/lh-10}} {{#text-margin}}mr-50{{/text-margin}}">{{&subCase}}</span>{{/subCase-header}}\
										{{#subCase-header}}<span class="w-100 black m-a {{#lh-10}}lh-10{{/lh-10}} {{#text-margin}}mr-50{{/text-margin}}">{{&subCase-header}}</span>{{/subCase-header}}\
									</div>{{/hide-subfeature-header}}{{/subCases}}\
								</div>\
							</div>\
							{{/show}}\
						</div>\
						<div class="{{#show}}sub-pos{{/show}} hidden-xs hidden-md hidden-sm hide-lg">\
							<div class="row clearfix-row">\
								<div class="col-md-12 col-xs-12 col-sm-12 clearfix-col">\
									{{#heading}}<p class="tc heading mt-62 heading-text {{heading-case}}">{{&heading}}</p>{{/heading}}\
									<p class="tc clearfix-p m-0 subheading black {{#isPerformance}}subheading-per{{/isPerformance}} {{#isRecommend}}recommend{{/isRecommend}}">{{sub-heading}}</p>\
								</div>\
							</div>\
							{{#show}}\
							<div class="row clearfix-row mb-53 text-center">\
								<div class="v-align text-center sub-feature">\
									{{#subCases}}{{^hide-subfeature-header}}<div class="mr-160 sub-features" onclick="showSubCase(\'{{id-1920}}\')">\
										<div class="mb-20">\
											<img src="{{subCase-img}}" class="mw-100 responsive-img">\
										</div>\
										{{^subCase-header}}<span class="w-100 black m-a {{#lh-10}}lh-10{{/lh-10}} {{#text-margin}}mr-50{{/text-margin}}">{{&subCase}}</span>{{/subCase-header}}\
										{{#subCase-header}}<span class="w-100 black m-a {{#lh-10}}lh-10{{/lh-10}} {{#text-margin}}mr-50{{/text-margin}}">{{&subCase-header}}</span>{{/subCase-header}}\
									</div>{{/hide-subfeature-header}}{{/subCases}}\
								</div>\
							</div>\
							{{/show}}\
						</div>\
						<div class="hidden-sm hidden-md hidden-lg mob nav-hide {{#show}}sub-pos{{/show}}">\
							<div class="row clearfix-row">\
								<div class="col-xs-12 clearfix-col">\
									<p class="tc clearfix-p mtb-62-30 heading">{{heading}}</p>\
								</div>\
							</div>\
							{{#show}}\
							<div class="row clearfix-row">\
								<div class="clearfix-col subfeature-header text-center">\
									{{#subCases}}{{^hide-subfeature-header}}<div class="p-0 mb-10" onclick="showSubCase(\'{{id-mob}}\')">\
										<div class="mb-10">\
											<img src="{{subCase-img}}" class="mw-40p">\
										</div>\
										{{^subCase-header}}<span class="w-100 {{#lh-10}}lh-10{{/lh-10}} black">{{subCase}}</span>{{/subCase-header}}\
										{{#subCase-header}}<span class="w-100 {{#lh-10}}lh-10{{/lh-10}} black">{{&subCase-header}}</span>{{/subCase-header}}\
									</div>{{/hide-subfeature-header}}{{/subCases}}\
								</div>\
							</div>\
							{{/show}}\
						</div>';

var sub_cases_template = '<div>\
							{{!css for desktop view}}\
							<div class="container-fluid clearfix-col hidden-xs hidden-xl">\
								{{#subCases}}{{^isSubCaseNotExist}}\
								<div class="row clearfix-row v-align {{background}} {{^FullImage}}h-461{{/FullImage}}" id="{{id}}">\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs clearfix-col">\
										{{#odd}}<div class="{{#odd}}ml-50 mr-80{{/odd}}">\
											{{#subCase-img}}{{^is-subCase-hide}}<div class="subfeature-img">\
												<img src="{{subCase-img}}" class="mw-100">\
											</div>{{/is-subCase-hide}}{{/subCase-img}}\
											<p class="clearfix-p subfeature feature-title mtb-30">{{subCase}}</p>\
											<ul class="features-9">\
											 	{{#subCaseList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle inner-circle-pos"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subCaseList}}\
											 	{{#scoring-graph}}<img src="{{lead-scoring}}" class="mw-100 graph-position">{{/scoring-graph}}\
											</ul>\
										</div>{{/odd}}\
										{{^odd}}<div>\
											<img src="{{img}}" class="mw-100">\
										</div>{{/odd}}\
									</div>\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs clearfix-col {{^FullImage}}mr-100{{/FullImage}}}">\
										{{^odd}}<div class="{{^odd}}mr-100 mlr-50{{/odd}} {{^FullImage}}mr-0{{/FullImage}}">\
											{{#subCase-img}}{{^is-subCase-hide}}<div class="subfeature-img">\
												<img src="{{subCase-img}}" class="mw-100 {{^FullImage}}mw-80{{/FullImage}}">\
											</div>{{/is-subCase-hide}}{{/subCase-img}}\
											<p class="clearfix-p feature-title mtb-30">{{subCase}}</p>\
											<ul class="features-9">\
											 	{{#subCaseList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle {{#per-inner}}per-inner{{/per-inner}} inner-circle-pos"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subCaseList}}\
											</ul>\
										</div>{{/odd}}\
										{{#odd}}<div>\
											<img src="{{img}}" class="mw-100">\
										</div>{{/odd}}\
									</div>\
								</div>\
								{{/isSubCaseNotExist}}{{/subCases}}\
							</div>\
							{{!css for large screen view >1600px}}\
							<div class="container-fluid clearfix-col hidden-xs hidden-sm hide-lg hidden-md">\
								{{#subCases}}{{^isSubCaseNotExist}}\
								<div class="row clearfix-row v-align {{background}} {{^FullImage-1920}}h-461{{/FullImage-1920}} {{#h-561}}h-680{{/h-561}}" id="{{id-1920}}">\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs clearfix-col">\
										{{#odd}}<div class="{{#odd}}ml-152 mtb-100 {{#mb-80}}mb-80{{/mb-80}} {{#mt-0}}mt0{{/mt-0}} {{#mtb-0}}mtb0{{/mtb-0}} mr-80 {{#mr-10}}mr-10{{/mr-10}}{{/odd}}">\
											{{#subCase-img}}{{^is-subCase-hide}}<div class="subfeature-img">\
												<img src="{{subCase-img}}" class="mw-100">\
											</div>{{/is-subCase-hide}}{{/subCase-img}}\
											<p class="clearfix-p subfeature feature-title {{#w-82}}w-82{{/w-82}} mtb-30 {{#mt-0}}m0{{/mt-0}}">{{subCase}}</p>\
											<ul class="features-9">\
											 	{{#subCaseList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle inner-circle-pos"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subCaseList}}\
											 	{{#scoring-graph}}<img src="{{lead-scoring}}" class="mw-200 graph-position">{{/scoring-graph}}\
											</ul>\
										</div>{{/odd}}\
										{{^odd}}<div class="">\
											<img src="{{img}}" class="mw-100  {{#FullImage-1920}}w-960{{/FullImage-1920}}">\
										</div>{{/odd}}\
									</div>\
									<div class="col-md-6 col-sm-6 col-lg-6 hidden-xs clearfix-col {{^FullImage-1920}}mr-100{{/FullImage-1920}}">\
										{{^odd}}<div class="{{^odd}}mr-200 mtb-100 {{#mb-80}}mb-80{{/mb-80}} {{#mt-0}}mt0{{/mt-0}} {{#mtb-0}}mtb0{{/mtb-0}} ml-90 mr-150{{/odd}}">\
											{{#subCase-img}}{{^is-subCase-hide}}<div class="subfeature-img">\
												<img src="{{subCase-img}}" class="mw-100">\
											</div>{{/is-subCase-hide}}{{/subCase-img}}\
											<p class="clearfix-p feature-title {{#w-82}}w-82{{/w-82}} mtb-30 {{#mt-0}}m0{{/mt-0}}">{{subCase}}</p>\
											<ul class="features-9">\
											 	{{#subCaseList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle {{#per-inner}}per-inner{{/per-inner}} inner-circle-pos-rt"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subCaseList}}\
											</ul>\
										</div>{{/odd}}\
										{{#odd}}<div class="">\
											<img src="{{img}}" class="mw-100 {{#FullImage-1920}}w-960{{/FullImage-1920}}">\
										</div>{{/odd}}\
									</div>\
								</div>\
								{{/isSubCaseNotExist}}{{/subCases}}\
							</div>\
							{{!css for mobile view}}\
							<div class="container-fluid clearfix-col hidden-lg hidden-md hidden-sm nav-hide hidden-xl">\
								{{#subCases}}{{^isSubCaseNotExist}}\
								<div class="row clearfix-row v-align {{background}}" id="{{id-mob}}">\
									<div class="col-xs-12 hidden-sm hidden-md hidden-lg clearfix-col">\
										<div>\
											{{#subCase-img}}{{^is-subCase-hide}}<p class="clearfix-p feature-title mtb-30 flex"><img src="{{subCase-img}}" class="mw-40p mr-10">{{subFeature}}</p>{{/is-subCase-hide}}{{/subCase-img}}\
											<ul class="features-9">\
											 	{{#subCaseList}}{{#isListExist}}<div class="webkit-inline"><p class="clearfix-p circle"></p><p class="clearfix-p inner-circle {{#per-inner}}per-inner{{/per-inner}} inner-circle-pos-rt"></p><li class="ml-10">{{{list}}}</li></div>{{/isListExist}}{{^isListExist}}<li class="">{{{list}}}</li>{{/isListExist}}{{/subCaseList}}\
											</ul>\
										</div>\
										<div>\
											<img src="{{img}}" class="mw-100">\
										</div>\
									</div>\
								</div>\
								{{/isSubCaseNotExist}}{{/subCases}}\
							</div>\
						</div>';

