// renders common footer for all the pages
function renderFooter(){
	jQuery("#common-footer").html(Mustache.render(common_footer));
	jQuery("#feature-footer").html(Mustache.render(feature_footer));
	// show color image on hover and show gray image on hover out
	jQuery(".banks").hover(function(){
		var bank = jQuery(this).find("img").attr('src');
		bank = bank.split('/')[7];
		bank = bank.split('-')[0];
		jQuery(this).find("img").attr('src','http://159.89.156.150/wp-content/uploads/2018/03/' + bank + '-color.png');
	},function(){
		var bank = jQuery(this).find("img").attr('src');
		bank = bank.split('/')[7];
		bank = bank.split('-')[0];
		jQuery(this).find("img").attr('src','http://159.89.156.150/wp-content/uploads/2018/03/' + bank + '-grey.png');
	});
}

jQuery(document).ready(function(){
	setMobileMenu();
});


var common_footer = '<div>\
						<div class="row clearfix-row">\
							<div class="clearfix-col col-md-12 col-sm-12 col-xs-12 col-lg-12">\
								<div>\
									<p class="clearfix-p text-center black happy-clients">Our Happy Clients</p>\
								</div>\
							</div>\
						</div>\
						<div class="container-fluid carousel-container our-clients-carousel">\
						  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">\
						    <!-- Indicators -->\
						    <ol class="carousel-indicators bank-carousel">\
						      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>\
						      <li data-target="#myCarousel" data-slide-to="1"></li>\
						    </ol>\
						    <!-- Wrapper for slides -->\
						    <div class="carousel-inner carousel-width text-center">\
						      	<div class="item active">\
							      	<div class="row clearfix-row">\
										<div class="clearfix-col col-md-12 col-sm-12 col-xs-12 col-lg-12">\
											<div>\
												<p class="clearfix-p lorem text-center m-a">"Working with GameChange Solutions has been a truly digital and innovative experience. The team has the potential to deliver business value with the most innovative use of the technology solutions. We look forward to working with GameChange Solutions and will recommend the same to other companies who have an appetite for growth."</p>\
											</div>\
										</div>\
									</div>\
						      	</div>\
							    <div class="item">\
							      	<div class="row clearfix-row">\
										<div class="clearfix-col col-md-12 col-sm-12 col-xs-12 col-lg-12">\
											<div>\
												<p class="clearfix-p lorem text-center m-a">"GameChange Solution\'s BitNudge application allows all sales users to use all CRM functionalities from the field, allowing the sales agents to be on-the ground for most of the time in the day. Productivity of my Direct Sales team has gone up by more than 10% during a three month pilot of BitNudge"</p>\
											</div>\
										</div>\
									</div>\
							    </div>\
						    </div>\
						  </div>\
						</div>\
						<div class="row clearfix-row bank-background">\
							<div class="col-md-12 col-sm-12 col-xs-12 clearfix-col show-banks">\
								<div class="banks">\
									<img src="http://159.89.156.150/wp-content/uploads/2018/03/mashreq-grey.png" class="mw-100 responsive-img bank">\
								</div>\
								<div class="banks">\
									<img src="http://159.89.156.150/wp-content/uploads/2018/03/meraas-grey.png" class="mw-100 responsive-img bank">\
								</div>\
								<div class="banks">\
									<img src="http://159.89.156.150/wp-content/uploads/2018/03/rakbank-grey.png" class="mw-100 responsive-img bank">\
								</div>\
								<div class="banks">\
									<img src="http://159.89.156.150/wp-content/uploads/2018/03/tanfeet-grey.png" class="pw-100 responsive-img bank">\
								</div>\
								<div class="banks">\
									<img src="http://159.89.156.150/wp-content/uploads/2018/03/tata-grey.png" class="mw-100 responsive-img bank">\
								</div>\
								<div class="banks">\
									<img src="http://159.89.156.150/wp-content/uploads/2018/03/adib-grey.png" class="mw-100 responsive-img bank">\
								</div>\
								<div class="banks">\
									<img src="http://159.89.156.150/wp-content/uploads/2018/03/careem-grey.png" class="mw-100 responsive-img bank">\
								</div>\
							</div>\
						</div>\
					</div>'

var feature_footer = '<div>\
						<div class="row clearfix-row">\
							<div class="col-md-12 col-sm-12 col-xs-12 clearfix-col">\
								<div class="feature-footer">\
									<div>\
										<p class="deliver mb-30">We deliver quick, customizable and user-centric Field Sales Productivity!</p>\
										<p class="mb-30 want">Want to know more? Get in touch directly!</p>\
										<div><input type="email" placeholder="Email address" class="email"/><button class="submit" onclick="sendMail();">Send</button></div>\
										<div class="userMessage"></div>\
									</div>\
								</div>\
							</div>\
						</div>\
					  </div>';