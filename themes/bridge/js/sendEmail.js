// function to send email 
function sendMail(){
  // variable intialization
	var ajax = getXmlHttpObject();
	var url = '/sendEmail.php';
	var email;
  $required_email_message = "Email is required";
  $valid_email_message = "Please enter a valid email";
	email = jQuery('.email').val();

  // checks whether email is empty or not
  if (!email || email === 'undefined' || email === 'null') {
    jQuery('.userMessage').text($required_email_message);
    setTimeout(function(){
      jQuery('.userMessage').text('');
    },5000);
    return;
  }
  $email_validate = validateEmail(email);

  // checks whether entered email format is valid or not
  if (!$email_validate) {
    jQuery('.userMessage').text($valid_email_message);
    setTimeout(function(){
      jQuery('.userMessage').text('');
    },5000);
    return;
  }
  email = email.trim();
	var postData = 'form-type=contact&email=' + email;
	ajax.onreadystatechange = function() {
    if(ajax && ajax.readyState == 4){
     	if(ajax && ajax.status === 200){
        jQuery('.email').val("");
        jQuery('.userMessage').text("Thanks for contacting us, we will get back to you soon !");
        setTimeout(function(){
          jQuery('.userMessage').text('');
        },5000);
     	} else{
        jQuery('.userMessage').text("Sorry, we are not able to send your email at this moment, Please try some other time");
        setTimeout(function(){
          jQuery('.userMessage').text('');
        },5000);
      }
   	}
  }
  ajax.sendAsyncRequest(url, postData);
}

// Regex to validate email format
function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function getXmlHttpObject() {
   var xmlHttp;
   if (window.XMLHttpRequest) {
       xmlHttp = new XMLHttpRequest();
   } else {
       xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
   }
   xmlHttp.sendAsyncRequest = function(url, data) {
       this.open("POST", url, true);
       this.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
       this.send(data);
   };
   return (xmlHttp);
}

// send mail from contact form
function sendContactMail() {
  var ajax = getXmlHttpObject();
  var url = '/sendContactMail.php';
  var name, email, subject, message;
  $required_email_message = "Email is required";
  $valid_email_message = "Please enter a valid email";
  $COMPLETE_ALL_FIELD = "Please fill all the fields";
  $NAME_REQUIRED = "Name is required";
  $VALID_NAME = "Please enter a valid name";
  $MESSAGE_REQUIRED = "Message is required";
  $SUBJECT_REQUIRED = "Subject is required";
  $SUCCESS = "Thanks for contacting us, we will get back to you soon !";
  $FAILED = "Sorry, we are not able to send your email at this moment, Please try some other time";
  name = jQuery('.name').val();
  console.log(name);
  email = jQuery('.contact-email').val();
  console.log(email);
  subject = jQuery('.subject').val();
  console.log(subject);
  message = jQuery('.message').val();
  if(!name && !email && !subject && !message){
    jQuery('.common-tooltip').text($COMPLETE_ALL_FIELD);
    // setTimeout(function(){
    //   jQuery('.common-tooltip').text('');
    // },5000);
    return;
  }
  if(!name || name === 'undefined' || name === 'null'){
    jQuery('.name-tooltip').text($NAME_REQUIRED);
    setTimeout(function(){
      jQuery('.name-tooltip').text('');
    },5000);
    return;
  }
  if(!email || email === 'undefined' || email === 'null'){
    jQuery('.email-tooltip').text($required_email_message);
    setTimeout(function(){
      jQuery('.email-tooltip').text('');
    },5000);
    return;
  }
  $email_valid = validateEmail(email) 
  if (!$email_valid) {
    jQuery('.email-tooltip').text($required_email_message);
    setTimeout(function(){
      jQuery('.email-tooltip').text('');
    },5000);
    return;
  }
  if(!message || message === 'undefined' || message === 'null') {
    jQuery('.message-tolltip').text($MESSAGE_REQUIRED);
    setTimeout(function(){
      jQuery('.message-tolltip').text('');
    },5000);
    return;
  }
  if (!subject || subject === 'undefined' || subject === 'null') {
    jQuery('.subject-tolltip').text($SUBJECT_REQUIRED);
    setTimeout(function(){
      jQuery('.subject-tolltip').text('');
    },5000);
    return;
  }
  name = name.trim();
  email = email.trim();
  subject = subject.trim();
  message = message.trim();
	var postData = 'form-type=contact&name=' + name + '&email=' + email;
  postData += '&subject=' + subject;
  postData += '&message=' + message;
 	ajax.onreadystatechange = function() {
    if(ajax && ajax.readyState == 4){
      if(ajax && ajax.status === 200){
        jQuery('.name').val("");
        jQuery('.contact-email').val("");
        jQuery('.subject').val("");
        jQuery('.message').val("");
        jQuery('.common-tooltip').text($SUCCESS);
        setTimeout(function(){
          jQuery('.common-tooltip').text('');
        },5000);
      }else{
        jQuery('.common-tooltip').text($FAILED);
        setTimeout(function(){
          jQuery('.common-tooltip').text('');
        },5000);
      }
    }
 	}
 	ajax.sendAsyncRequest(url, postData);
}

jQuery('document').ready(function(){
	
})