function setAboutUsData(){
	jQuery.getJSON('../wp-content/themes/bridge/js/about-us.json',function(data) {
	    result = data;
	    jQuery("#sub-list").html(Mustache.render(sub_feature_template,result));
	}); 
	renderFooter();
	jQuery(".main_menu").css({'left':'153px','margin-left':'40px;'})
	jQuery(".header_inner_right").append('<div class="demo-desk"><button>Request a Demo</button></div>');
	jQuery('.demo-desk').click(function(){
		window.location.href = '/#contact';
	});
	jQuery('.hero-contact').click(function(){
		console.log("into hero")
		window.location.href = '/#contact';
	})
}




jQuery(document).ready(function(){
	setAboutUsData();
})

// template for about us page sub features

var sub_feature_template = '<div class="row clearfix-row hidden-xs">\
								{{#sub-list}}<div class="col-md-12 col-sm-12 col-lg-12 clearfix-col v-align">\
									<div class="col-md-6 col-sm-6 col-lg-6 clearfix-col">\
										{{#odd}}<p class="clearfix-p about-us-list">{{&content}}</p>{{/odd}}\
										{{^odd}}<img src="{{img}}" class="w-957 responsive-img">{{/odd}}\
									</div>\
									<div class="col-md-6 col-sm-6 col-lg-6 clearfix-col">\
										{{^odd}}<p class="clearfix-p about-us-list">{{&content}}</p>{{/odd}}\
										{{#odd}}<img src="{{img}}" class="w-957 responsive-img">{{/odd}}\
									</div>\
								</div>{{/sub-list}}\
							</div>\
							<div class="row clearfix-row hidden-sm hidden-lg hidden-md nav-hide">\
								{{#sub-list}}<div class="col-xs-12 clearfix-col v-align">\
									<div class="col-md-6 col-sm-6 col-lg-6 clearfix-col">\
										<p class="clearfix-p about-us-list">{{&content}}</p>\
										<img src="{{img}}" class="w-957 responsive-img">\
									</div>\
								</div>{{/sub-list}}\
							</div>'

