<?php get_header()?>
<div class="blog-head-bg">
	<h1 class="blog-header-text hideInMobile">GameChange Solutions Blog <br>Curated content on productivity improvement, engagement<br>and capability building</h1>
	<h1 class="blog-header-text showInMobile">GameChange Solutions Blog <br>Curated content on productivity improvement, engagement<br>and capability building</h1>
	<div class="blog-features-div">
		<h4 class="blogHeaderLabel">Featured Content</h4>
	<?php echo do_shortcode('[wonderplugin_carousel id="1"]'); ?>
	</div>
</div>
<div class="bodyBgBlog" style="margin-top: 5rem;margin-bottom: 5rem">
<?php echo do_shortcode('[wonderplugin_gridgallery id=1]'); ?>
<?php echo do_shortcode('[wonderplugin_gridgallery id=2]'); ?>
 <?php echo do_shortcode('[wonderplugin_gridgallery id=3]'); ?>
</div>
<div class="blog-popular-div">
		<h4 class="blogPopularHeaderLabel">Popular Videos</h4>
 <?php echo do_shortcode('[wonderplugin_carousel id="2"]'); ?>
 </div>
 <div class="bodyBgBlog latestBlogDiv">
 <h4 class="blogHeaderLabel latestBlogLabel">Latest Blog Posts</h4>
 <?php echo do_shortcode('[wonderplugin_carousel id="3"]'); ?>
 </div>
 <div id="common-footer" style="background-color: #fff">
 </div>
 <div id="feature-footer">
 </div>
 <script type="text/javascript" src="../../../wp-content/themes/bridge/js/mustache.js">
 </script>
 <script type="text/javascript" src="../../../wp-content/themes/bridge/js/footer.js">
 </script>
 <script type="text/javascript">
 	renderFooter()
 	jQuery(".header_inner_right").append('<div class="demo-desk"><button>Request a Demo</button></div>');
	jQuery('.demo-desk').click(function(){
		window.location.href = '/#contact';
	});
 </script>
<?php get_footer()?>