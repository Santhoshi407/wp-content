<?php get_header() ?>
<section class="background-white">
	<div class="row home-background pos-rel">
		<div class="hero-text">
			<p class="clearfix-p home-innovation text-center">Innovation at Core!</p>
			<p class="clearfix-p amplifying-engagement text-center">Amplifying Engagement,Driving Performance.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 clearfix-col home-banks hidden-xs">
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/adib-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/rakbank-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/careem-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/tanfeeth-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/meeras-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/mashreq-h-96-1.png" class="responsive-img bank h-48">
			</div>
			<div class="v-align">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/emaar-h-96-1.png" class="responsive-img bank h-48">
			</div>
		</div>
		<div id="banks-carousel" class="carousel slide hidden-sm hidden-xl hidden-lg hidden-md" data-ride="carousel">
		    <!-- Indicators -->
		    <ol class="carousel-indicators bank-carousel mob-bank-carousel">
		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		      <li data-target="#myCarousel" data-slide-to="1"></li>
		      <li data-target="#myCarousel" data-slide-to="2"></li>
		      <li data-target="#myCarousel" data-slide-to="3"></li>
		      <li data-target="#myCarousel" data-slide-to="4"></li>
		      <li data-target="#myCarousel" data-slide-to="5"></li>
		      <li data-target="#myCarousel" data-slide-to="6"></li>
		      <li data-target="#myCarousel" data-slide-to="7"></li>
		    </ol>

		    <!-- Wrapper for slides -->
		    <div class="carousel-inner">
		      	<div class="item active">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/adib-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/rakbank-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/careem-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/tanfeeth-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/meeras-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/mashreq-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      	<div class="item">
			        <img src="http://159.89.156.150/wp-content/uploads/2018/04/emaar-h-96-1.png" class="responsive-img bank h-48">
		      	</div>
		      </div>
		    </div>
		  </div>
	</div>
	<div class="row performance-row">
		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 text-center pt-180">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div>
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/performance_360.png" class="mw-100">
				</div>
				<div class="performance-360">
					<p>Performance 360 degree</p>
				</div>
				<div class="performance-360-text">
					<p>Our core platform. BitNudge, enables 360“ performance management, powered with Advanced Analutics, Gamification, Nudge theory etc.,</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div>
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/bots.png" class="mw-100">
				</div>
				<div class="performance-360">
					<p>Enterprise AI Bots</p>
				</div>
				<div class="performance-360-text">
					<p>Custom built for businesses for virtual assistance,learning, business intelligence etc., on latest platforms such as Alexa, Cortana, Google Now.</p>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center pt-120">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div>
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/smart_activity.png" class="mw-100">
				</div>
				<div class="performance-360">
					<p>Smart Activity Tracking</p>
				</div>
				<div class="performance-360-text">
					<p>Tracking of daily sales activities made easy & nimble by enabling multiple interfaces for fleet on street mining, ssuch as voice, image, NFC & custom keyboard etc.,</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div>
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/advanced_analytics.png" class="mw-100">
				</div>
				<div class="performance-360">
					<p>Advanced Analytics</p>
				</div>
				<div class="performance-360-text">
					<p>Advanced analytics platform with predictive analytics, data mining, big data analytics & location intelligence capabilities. AI-enabled Alerts & Recommendations on BitNudge drive desired results.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="row mb-50 mt-200 bg-functional-use">
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<p class="clearfix-p text-center functional-case">Functional Use Cases</p>
		  </div>
		  <div id="functional-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
		  	<!-- Indicators -->
		    <ol class="carousel-indicators functional-buttons-pos">
		      <li data-target="#functional-carousel" data-slide-to="o" class="active pos-rel">SALES</li>
		      <li data-target="#functional-carousel" data-slide-to="1" class="pos-rel">OPERATIONS</li>
		      <li data-target="#functional-carousel" data-slide-to="2" class="pos-rel">CALL CENTER</li>
		    </ol>
		    <!-- Wrapper for slides -->
		    <div class="carousel-inner pos-rel">
			    <div class="item active sales-pos">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/sales_img-1.png" class="mw-100 responsive-img">
			    </div>
		      	<div class="item sales-pos">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/gamification_img.png" class="mw-100 responsive-img">
		      	</div>
		      	<div class="item sales-pos">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/Callcenter_img.png" class="mw-100 responsive-img">
		      	</div>
		    </div>
		    <!-- Left and right controls -->
		    <a class="left carousel-control v-align" href="#functional-carousel" data-slide="prev">
		      <img src="http://159.89.156.150/wp-content/uploads/2018/04/left_arrow.png" class="mw-100 responsive-img">
		      <span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control v-align" href="#functional-carousel" data-slide="next">
		      <img src="http://159.89.156.150/wp-content/uploads/2018/04/right_arrow.png" class="mw-100 responsive-img">
		      <span class="sr-only">Next</span>
		    </a>
		  </div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-lg-12"> 
				<p class="functional-cases-text">Our customizable Sales management platform improves the performance of frontline sales agents by leveraging the power of Advanced Analytics, latest technology and Gamification etc. through</p>
			</div>
			<div class="col-md-12 col-xs-12 col-sm-12 mt-60 mb-80 functionalities">
				<div class="col-md-2 col-sm-2 col-lg-2 hidden-xs">
				</div>
				<ol class="col-md-4 col-sm-4 col-lg-4 col-xs-6 PIG">
					<li>Planning engine </li>
					<li>Intelligent Lead and Activity management </li>
					<li class="w-60">Gamification (competition, campaign, badges, SWOT etc.) </li>
				</ol>
				<ol class="col-md-4 col-sm-4 col-lg-4 col-xs-6">
					<li>Incentives and compensation management</li>
					<li>Learning</li>
					<li>Automation and Analytics </li>
				</ol>
				<div class="col-md-2 col-sm-2 col-lg-2 hidden-xs">
				</div>
			</div>
		</div>
	</div>
	
	<div class="row background-ghost-white h-595 v-align">
		<div class="col-md-12 col-xs-12 xol-sm-12 col-lg-12 innovation-core">
			<div class="pb-120">
				<p class="clearfix-p text-center features-core">Bitnudge - Innovation at the core</p>
			</div>
			<div class="features-arr">
				<div onclick="window.location.href='http://159.89.156.150/lead-management/'">
					<div class="pos-rel"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/lead_management_icon.png" class="mw-100 responsive-img">
						<div class="feature-lines hidden-xs"></div>
					</div>
					<p class="clearfix-p text-center feature-names" style="width: 50%;">Lead Management</p>
				</div>
				<div onclick="window.location.href='http://159.89.156.150/lead-scoring-and-allocation/'">
					<div class="pos-rel"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/lead_scoring_icon.png" class="mw-100 responsive-img">
						<div class="feature-lines hidden-xs"></div>
					</div>
					<p class="clearfix-p text-center feature-names">Lead Scoring and Allocation</p>
				</div>
				<div onclick="window.location.href='http://159.89.156.150/performance-management/'">
					<div class="pos-rel"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/performance_management_icon.png" class="mw-100 responsive-img">
						<div class="feature-lines hidden-xs"></div>
					</div>
					<p class="clearfix-p text-center feature-names">Performance Management</p>
				</div>
				<div onclick="window.location.href='http://159.89.156.150/gamification/'">
					<div class="pos-rel"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/gamification_icon.png" class="mw-100 responsive-img">
						<div class="feature-lines hidden-xs"></div>
					</div>
					<p class="clearfix-p text-center feature-names">Gamification</p>
				</div>
				<div onclick="window.location.href='http://159.89.156.150/recommendation-engine/'">
					<div class="pos-rel"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/recommendation_icon.png" class="mw-100 responsive-img">
						<div class="feature-lines hidden-xs"></div>
					</div>
					<p class="clearfix-p text-center feature-names">Recommendation Engine</p>
				</div>
				<div onclick="window.location.href='http://159.89.156.150/social-channel/'">
					<div class="pos-rel"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/enterprise_social_icon.png" class="mw-100 responsive-img">
						<div class="feature-lines hidden-xs"></div>
					</div>
					<p class="clearfix-p text-center feature-names">Enterprise Social Channel and Rewards</p>
				</div>
				<div onclick="window.location.href='http://159.89.156.150/social-selling/'">
					<div class="pos-rel"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/social_selling_icon.png" class="mw-100 responsive-img">
						<div class="feature-lines hidden-xs"></div>
					</div>
					<p class="clearfix-p text-center feature-names">Social Selling and Digital Onboarding</p>
				</div>
				<div onclick="window.location.href='http://159.89.156.150/geo-tagging/'">
					<div class="pos-rel"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/geo_tagging_icon.png" class="mw-100 responsive-img">
						<div class="feature-lines hidden-xs"></div>
					</div>
					<p class="clearfix-p text-center feature-names">Geo-tagging/ location service</p>
				</div>
				<div onclick="window.location.href='http://159.89.156.150/frontline-learning/'">
					<div class="pos-rel"> 
						<img src="http://159.89.156.150/wp-content/uploads/2018/04/learning_management_icon.png" class="mw-100 responsive-img">
					</div>
					<p class="clearfix-p text-center feature-names">Frontline learning Management</p>
				</div>
			</div>
		</div>
	</div>
	<div class="stats-background v-align stats w-100">
		<div class="br-stats">
			<div class="text-right pr-100">
				<p class="stats-text mb-50">Daily Active Users</p>
				<p class="stats-count">4500</p>
			</div>
		</div>
		<div class="br-stats">
			<div class="text-center">
				<p class="stats-text mb-50">Users Served Since Inspection</p>
				<p class="stats-count">35000</p>
			</div>
		</div>
		<div class="">
			<div class="text-left pl-100">
				<p class="stats-text text-left mb-50 w-50">Hours of content served through the platform powered by LMS</p>
				<p class="stats-count text-left">2000</p>
			</div>
		</div>
	</div>
	<div class="row ptb-180 background-blog">
		<div> 
			<p class="clearfix-p text-center black home-blog-posts mb-100">Blog Posts</p>
		  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
		    <!-- Indicators -->
		    <ol class="carousel-indicators bank-carousel blog-carousel blogs-carousel">
		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		      <li data-target="#myCarousel" data-slide-to="1"></li>
		      <li data-target="#myCarousel" data-slide-to="2"></li>
		    </ol>

		    <!-- Wrapper for slides -->
		    <div class="carousel-inner">
		      	<div class="item active">
			        <div class="row ptb-200">
			        	<div class="col-md-4 col-sm-4 col-lg-4 clearfix-col">
			        		<div class="blogs">
				        		<img src="http://159.89.156.150/wp-content/uploads/2018/04/blog_img01.png" class="mw-100">
				        		<p class="clearfix-p blog-pots-title black pl-70">Gamification at work</p>
				        		<p class="clearfix-p blog-post-text pl-70">Gamification is a tool to encourage people to change their behavior by positive re-enforcements. While doing so </p>
			        		</div>
			        	</div>
			        	<div class="col-md-4 col-sm-4 col-lg-4 clearfix-col">
			        		<div class="blogs">
				        		<img src="http://159.89.156.150/wp-content/uploads/2018/04/blog_img02.png" class="mw-100">
				        		<p class="clearfix-p blog-pots-title black pl-70">Root cause of salesforce...</p>
				        		<p class="clearfix-p blog-post-text pl-70">Employee engagement, employee learning and development and performance management are among the key challenges for an organization</p>
			        		</div>
			        	</div>
			        	<div class="col-md-4 col-sm-4 col-lg-4 clearfix-col">
			        		<div class="blogs">
				        		<img src="http://159.89.156.150/wp-content/uploads/2018/04/blog_img03.png" class="mw-100">
				        		<p class="clearfix-p blog-pots-title black pl-70">Why improve employee...</p>
				        		<p class="clearfix-p blog-post-text pl-70">In today's modern world, where an individual's creativity, innovation and entrepreneurship are required in their job, it is very important to keep </p>
			        		</div>
			        	</div>
			        </div>
		      	</div>
		      	<div class="item">
			        <div class="row ptb-200">
			        	<div class="col-md-4 col-sm-4 col-lg-4 clearfix-col">
			        		<img src="http://159.89.156.150/wp-content/uploads/2018/04/blog_img02.png" class="mw-100">
			        		<p class="clearfix-p blog-pots-title black pl-70">Root cause of salesforce...</p>
			        		<p class="clearfix-p blog-post-text pl-70">Employee engagement, employee learning and development and performance management are among the key challenges for an organization</p>
			        	</div>
			        	<div class="col-md-4 col-sm-4 col-lg-4 clearfix-col">
			        		<img src="http://159.89.156.150/wp-content/uploads/2018/04/blog_img03.png" class="mw-100">
			        		<p class="clearfix-p blog-pots-title black pl-70">Why improve employee...</p>
			        		<p class="clearfix-p blog-post-text pl-70">In today's modern world, where an individual's creativity, innovation and entrepreneurship are required in their job, it is very important to keep </p>
			        	</div>
			        	<div class="col-md-4 col-sm-4 col-lg-4 clearfix-col">
			        		<img src="http://159.89.156.150/wp-content/uploads/2018/04/blog_img01.png" class="mw-100">
			        		<p class="clearfix-p blog-pots-title black pl-70">Gamification at work</p>
			        		<p class="clearfix-p blog-post-text pl-70">Gamification is a tool to encourage people to change their behavior by positive re-enforcements. While doing so </p>
			        	</div>
			        </div>
		      	</div>
		      	<div class="item">
			        <div class="row ptb-200">
			        	<div class="col-md-4 col-sm-4 col-lg-4 clearfix-col">
			        		<img src="http://159.89.156.150/wp-content/uploads/2018/04/blog_img03.png" class="mw-100">
			        		<p class="clearfix-p blog-pots-title black pl-70">Why improve employee...</p>
			        		<p class="clearfix-p blog-post-text pl-70">In today's modern world, where an individual's creativity, innovation and entrepreneurship are required in their job, it is very important to keep </p>
			        	</div>
			        	<div class="col-md-4 col-sm-4 col-lg-4 clearfix-col">
			        		<img src="http://159.89.156.150/wp-content/uploads/2018/04/blog_img01.png" class="mw-100">
			        		<p class="clearfix-p blog-pots-title black pl-70">Gamification at work</p>
			        		<p class="clearfix-p blog-post-text pl-70">Gamification is a tool to encourage people to change their behavior by positive re-enforcements. While doing so </p>
			        	</div>
			        	<div class="col-md-4 col-sm-4 col-lg-4 clearfix-col">
			        		<img src="http://159.89.156.150/wp-content/uploads/2018/04/blog_img02.png" class="mw-100">
			        		<p class="clearfix-p blog-pots-title black pl-70">Root cause of salesforce...</p>
			        		<p class="clearfix-p blog-post-text pl-70">Employee engagement, employee learning and development and performance management are among the key challenges for an organization</p>
			        	</div>
			        </div>
		      	</div>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
	<div class="row background-white ptb-180-130 our-team">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<p class="clearfix-p text-center black home-blog-posts mb-100">Our Team</p>
		</div>
		<div class="col-md-4 col-sm-4 col-lg-4 col-xs-12 text-center">
			<div class="our-team-mem">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic_01-1.png" class="responsive-img mw-100">
			</div>
			<p class="clearfix-p black team-text text-center">Shailesh Tiwari</p>
			<p class="clearfix-p text-center mtb-10-20 font18" style="color:#666666;">Founder and CEO </p>
			<p class="clearfix-p black text-center team-desc">In depth expertise In major verticals of Banking, Public Finance and Sovereign Wealth Funds. Led the Corporate and Investment Banking Practice for McKinsey and Co in ME. </p>
			<div class="v-align">
				<div class="social-icons mr-7">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/facebook-1.png" class="mw-100 responsive-img">
				</div>
				<div class="social-icons mr-7">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100 responsive-img">
				</div>
				<div class="social-icons">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/twitter-1.png" class="mw-100 responsive-img">
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-lg-4 col-xs-12 text-center">
			<div class="our-team-mem">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic_02.png" class="responsive-img mw-100">
			</div>
			<p class="clearfix-p black team-text text-center">Adhityan KV</p>
			<p class="clearfix-p text-center mtb-10-20 font18" style="color:#666666;">Cheif Technology Officer</p>
			<p class="clearfix-p black text-center team-desc">Over 8+ years of Industry expertise. Led engineering teams at OpenTable and Zomato. Was world's youngest Guiness certified windows programmer.</p>
			<div class="v-align">
				<div class="social-icons mr-7">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/facebook-1.png" class="mw-100 responsive-img">
				</div>
				<div class="social-icons mr-7">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100 responsive-img">
				</div>
				<div class="social-icons">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/twitter-1.png" class="mw-100 responsive-img">
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-lg-4 col-xs-12 text-center">
			<div class="our-team-mem">
				<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic_03.png" class="responsive-img mw-100">
			</div>
			<p class="clearfix-p black team-text text-center">Sumedh Wahane</p>
			<p class="clearfix-p text-center mtb-10-20 font18" style="color:#666666;">Chief Data Officer</p>
			<p class="clearfix-p black text-center team-desc">Transformation and Analytics Specialist with Consulting experience at McKinsey. Hands-on knowledge of delivering impact at Global clients through technology enablement. </p>
			<div class="v-align">
				<div class="social-icons mr-7">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/facebook-1.png" class="mw-100 responsive-img">
				</div>
				<div class="social-icons mr-7">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100 responsive-img">
				</div>
				<div class="social-icons">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/twitter-1.png" class="mw-100 responsive-img">
				</div>
			</div>
		</div>
	</div>
	<div class="row bg-learning text-center">
		<p class="clearfix-p text-center lr mt-270">Learning Resources</p>
		<div id="learning-resources" class="carousel slide" data-ride="carousel" data-interval="false">
		    <!-- Indicators -->
		    <ol class="carousel-indicators bank-carousel blog-carousel learning-carousel">
		      <li data-target="#learning-resources" data-slide-to="0" class="active"></li>
		      <li data-target="#learning-resources" data-slide-to="1"></li>
		      <li data-target="#learning-resources" data-slide-to="2"></li>
		    </ol>
		    <!-- Wrapper for slides -->
		    <div class="carousel-inner mt-70">
		      	<div class="item active">
		      		<div>
			      		<iframe width="340" height="187" src="https://www.youtube.com/embed/0MD4Ymjyc2I">
						</iframe>
					</div>
		      		<iframe width="770" height="430" src="https://www.youtube.com/embed/t__NoFstCmQ">
					</iframe>
					<div>
						<iframe width="340" height="187" src="https://www.youtube.com/embed/J5HriOFnL_0">
						</iframe>
					</div>
		      	</div>
		      	<div class="item v-align">
		      		<iframe width="340" height="187" src="https://www.youtube.com/embed/t__NoFstCmQ">
					</iframe>
		      		<iframe width="770" height="430" src="https://www.youtube.com/embed/J5HriOFnL_0" class="mrl-100">
					</iframe>
					<iframe width="340" height="187" src="https://www.youtube.com/embed/0MD4Ymjyc2I">
					</iframe>
		      	</div>
		      	<div class="item v-align">
		      		<iframe width="340" height="187" src="https://www.youtube.com/embed/J5HriOFnL_0">
					</iframe>
		      		<iframe width="770" height="430" src="https://www.youtube.com/embed/0MD4Ymjyc2I" class="mrl-100">
					</iframe>
					<iframe width="340" height="187" src="https://www.youtube.com/embed/t__NoFstCmQ">
					</iframe>
		      	</div>
		      </div>
		    </div>
		</div>
	</div>
	<div class="row bg-contact text-center">
		<p class="clearfix-p text-center contact-text">Contact us for a free consultation</p>
		<p class="clearfix-p text-center contact-sec-text">Dedication, Innovation, Value-add and everything in between.</p>
		<?php echo do_shortcode('[contact-form-7 id="1209" title="Untitled"]'); ?>
	</div>
	<div class="row bg-clients">
		<div> 
			<p class="clearfix-p text-center black home-blog-posts mtb-130">Our Happy Clients</p>
			<div id="happy-clients" class="carousel slide" data-ride="carousel" data-interval="false">
			    <!-- Indicators -->
			    <ol class="carousel-indicators bank-carousel blog-carousel">
			      <li data-target="#happy-clients" data-slide-to="0" class="active"></li>
			      <li data-target="#happy-clients" data-slide-to="1"></li>
			      <li data-target="#happy-clients" data-slide-to="2"></li>
			    </ol>

			    <!-- Wrapper for slides -->
			    <div class="carousel-inner">
			      	<div class="item active">
			      		<div class="bg-lorem pos-rel">
			      			<div class="lorem-pos black">
			      				<p class="clearfix-p black lorem-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			      				<img src="http://159.89.156.150/wp-content/uploads/2018/01/mashreq-color.png" class="mw-100 responsive-img">
			      			</div>
			      		</div>
			      	</div>
			      	<div class="item">
			      		<div class="bg-lorem pos-rel">
			      			<div class="lorem-pos black">
			      				<p class="clearfix-p black lorem-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			      				<img src="http://159.89.156.150/wp-content/uploads/2018/01/careem-color.png" class="mw-100 responsive-img">
			      			</div>
			      		</div>
			      	</div>
			      	<div class="item">
			      		<div class="bg-lorem pos-rel">
			      			<div class="lorem-pos black">
			      				<p class="clearfix-p black lorem-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			      				<img src="http://159.89.156.150/wp-content/uploads/2018/01/adib-color.png" class="mw-100 responsive-img">
			      			</div>
			      		</div>
			      	</div>
			    </div>
			    <!-- Left and right controls -->
			    <a class="left carousel-control v-align" href="#happy-clients" data-slide="prev">
			      <img src="http://159.89.156.150/wp-content/uploads/2018/04/left_arrow.png" class="mw-100 responsive-img">
			      <span class="sr-only">Previous</span>
			    </a>
			    <a class="right carousel-control v-align" href="#happy-clients" data-slide="next">
			      <img src="http://159.89.156.150/wp-content/uploads/2018/04/right_arrow.png" class="mw-100 responsive-img">
			      <span class="sr-only">Next</span>
			    </a>
			</div>
		</div>
	</div>
	<div id="feature-footer">
		<div>						
			<div class="row clearfix-row">							
				<div class="col-md-12 col-sm-12 col-xs-12 clearfix-col">								
					<div class="feature-footer">									
						<div>										
							<p class="deliver mb-30">We deliver quick, customizable and user-centric Field Sales Productivity!</p>	
							<p class="mb-30 want">Want to know more? Get in touch directly!</p>										
							<div>
								<input type="email" placeholder="Email address"><button class="submit" value="submit">Send</button>
							</div>									
						</div>								
					</div>							
				</div>						
			</div>					  
		</div>
	</div>
</section>
<?php get_footer() ?>
