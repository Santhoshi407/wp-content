<?php get_header()?>
	<section class="background-white">
		<div class="row bg-about pos-rel clearfix-row">
			<div class="our-company-bg">
				<p class="text-center our-company">Our Company</p>
				<p class="clearfix-p text-center about-us-hero">Built upon the philosophy of innovation and disruption, we are here to disrupt the existing inefficiencies in the enterprises.</p>
				<div class="text-center hero-contact">
					<button class="home-contact">Contact Us</button>
				</div>
			</div>
		</div>
		<div class="row clearfix-row overview">
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 clearfix-col">
				<p class="clearfix-p text-center about-us-title">Overview </p>
				<p class="clearfix-p text-center about-us-text">GameChange Solutions was established in 2014 with an objective of disrupting the existing performance management and employee engagement practices at financial institutions through innovative concepts from behavioral science powered with a state-of-the-art technology platform </p>
			</div>
		</div>
		<hr class="hr">
		<div class="row clearfix-row offices">
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 clearfix-col">
				<p class="clearfix-p text-center about-us-title">Our Offices  </p>
				<p class="clearfix-p text-center about-us-text">GameChange Solutions has its offices located in UAE, India and Singapore to deliver the value through proven tools and technology in a sustainable way through local presence</p>
				<div class="our-location">
					<img src="http://159.89.156.150/wp-content/uploads/2018/04/ourlocation_img.jpg" class="mw-100 responsive-img">
				</div>
			</div>
		</div>
		<hr class="hr">
		<div class="row clearfix-row founding-team">
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 clearfix-col">
				<p class="clearfix-p text-center about-us-text">The founding team brings an unrivalled mix of business and technology know-how through a unique mix of business, management consulting, advanced analytics, software, and gaming industries </p>
			</div>
		</div>
		<div class="row clearfix-row">
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 clearfix-col">
				<p class="clearfix-p text-center about-us-title">Leadership</p>
				<p class="clearfix-p text-center about-us-text leadership-text">Our team brings best combination of domain knowledge, solution design, analytics, and automation know-how </p>
				<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 about-leaders">
					<div class="col-md-3 col-xs-12 col-sm-3 col-lg-3 clearfix-col">
						<div class="team-align">
							<div>
								<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic01.jpg" class="mw-100 responsive-img">
							</div>
							<div class="about-linkedin" onclick="window.location.href='https://www.linkedin.com/in/shaileshtiwari'"><img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100"></div>
							<p class="about-team-text black">Shailesh Tiwari</p>
							<p class="team-details clearfix-p">Founder and CEO</p>
							<p class="team-details about-details">Ex McKinsey and Company Associate Partner; Led a key segment of Banking Practice for McKinsey and Company in Middle East. Over 10 years of deep industry expertise in Financial Services covering major verticals of Banking; core skills around Digital, Performance Transformation.</p>
						</div>
					</div>
					<div class="col-md-3 col-xs-12 col-sm-3 col-lg-3 clearfix-col">
						<div class="team-align">
							<div>
								<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic02.jpg" class="mw-100 responsive-img">
							</div>
							<div class="about-linkedin" onclick="window.location.href='https://www.linkedin.com/in/adhityan'"><img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100"></div>
							<p class="about-team-text black">Adhityan KV</p>
							<p class="team-details clearfix-p">Product & Technology Head</p>
							<p class="team-details about-details">Led the web engineering team of Zomato, through their series D and E (attracting investments of more than $500 million). Led payments engineering for OpenTable - a billion dollar tech venture based out of San Francisco. Was world's youngest certified professional windows application developer; won a Guiness record for the same.</p>
						</div>
					</div>
					<div class="col-md-3 col-xs-12 col-sm-3 col-lg-3 clearfix-col">
						<div class="team-align">
							<div>
								<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic03.jpg" class="mw-100 responsive-img">
							</div>
							<div class="about-linkedin" onclick="window.location.href='https://www.linkedin.com/in/sumedhwahane'"><img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100"></div>
							<p class="about-team-text black">Sumedh Wahane</p>
							<p class="team-details clearfix-p">Chief Data Officer</p>
							<p class="team-details about-details">Transformation and Analytics Specialist with deep Consulting and Advanced Analytics experience at McKinsey. Solid track record of delivering high impact solutions at Global clients. Core skills around machine learning and AI.</p>
						</div>
					</div>
					<div class="col-md-3 col-xs-12 col-sm-3 col-lg-3 clearfix-col">
						<div class="team-align">
							<div>
								<img src="http://159.89.156.150/wp-content/uploads/2018/04/team_pic04.jpg" class="mw-100 responsive-img">
							</div>
							<div class="about-linkedin" onclick="window.location.href='https://www.linkedin.com/in/mohitjandwani'"><img src="http://159.89.156.150/wp-content/uploads/2018/04/linkedin-1.png" class="mw-100"></div>
							<p class="about-team-text black">Mohit Jandwani</p>
							<p class="team-details clearfix-p">India Head </p>
							<p class="team-details about-details">Tech leader with proven expertise in building and running high quality tech teams. CTO at Babychakra, which is the largest network of mothers in India. Lead the POS initiative at Limetray which is the largest restaurant software provider in India. IIT Delhi graduate and top ranker in JEE, India's most competitive exam.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix-row background-blog">
			<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
				<p class="clearfix-p text-center about-experience">We enable organizations to leverage latest tools and techniques to derive tangible business impact, for example – use of Amazon Alexa platform to create Audio-Visual business intelligence bots for the enterprise, leveraging custom mobile keypads to boost agent sales productivity, and enabling social selling as well as to create customer-facing Swift keys in-order to boost the customer delight for enterprise</p>
			</div>
		</div>
		<div id ="sub-list">

		</div>
		<div>
		</div>
		<div id="common-footer">
		</div>
		<div id="feature-footer">
		</div>
	</section>
<?php get_footer()?>